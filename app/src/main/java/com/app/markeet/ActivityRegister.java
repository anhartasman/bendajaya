package com.app.markeet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Akun.response.responseLogin;
import com.app.markeet.model.Akun.response.responseRegister;
import com.app.markeet.utils.CallbackDialog;
import com.app.markeet.utils.DialogUtils;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityRegister extends AppCompatActivity {
    android.support.design.widget.TextInputLayout name_lyt=null;
    EditText name=null;
    android.support.design.widget.TextInputLayout email_lyt=null;
    EditText email=null;
    android.support.design.widget.TextInputLayout buyer_name_lyt=null;
    EditText username=null;
    android.support.design.widget.TextInputLayout password_lyt=null;
    EditText password=null;
    android.support.design.widget.TextInputLayout confirmpassword_lyt=null;
    EditText confirmpassword=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_submit=null;
    TextView tv_add_cart=null;
    private View parent_view;
    private Dialog dialog_failed = null;
    ThisApplication ta;
    ProgressDialog progressDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        parent_view = findViewById(android.R.id.content);
        ta = ((ThisApplication)getApplicationContext());


        email_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.email_lyt);
        email= (EditText)findViewById(R.id.email);
        name_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.name_lyt);
        name= (EditText)findViewById(R.id.name);
        buyer_name_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.buyer_name_lyt);
        username= (EditText)findViewById(R.id.username);
        password_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.password_lyt);
        password= (EditText)findViewById(R.id.password);
        confirmpassword_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.confirmpassword_lyt);
        confirmpassword= (EditText)findViewById(R.id.confirmpassword);
        lyt_submit= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_submit);
        tv_add_cart= (TextView)findViewById(R.id.tv_add_cart);

        progressDialog = new ProgressDialog(ActivityRegister.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_data));

        lyt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekValidRegister();
            }
        });
        //akhir onCreate
    }



    private void cekValidRegister() {
        int bisalogin=1;
        if (!validateUserName()) {
            Snackbar.make(parent_view, R.string.invalid_username, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validateName()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validateEmail()) {
            Snackbar.make(parent_view, R.string.invalid_email, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validatePassword()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (! password.getText().toString().equalsIgnoreCase(confirmpassword.getText().toString())) {
            Snackbar.make(parent_view, R.string.invalid_password_not_same, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }

        if(bisalogin==1){
            delayPostRegister();
        }


        // hide keyboard
        hideKeyboard();

    }

    private void delayPostRegister() {
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                postRegister();
            }
        }, 2000);
    }

    public void postRegister(){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","register");
        jsonObject.addProperty("name",name.getText().toString().trim());
        jsonObject.addProperty("email",email.getText().toString().trim());
        jsonObject.addProperty("username",username.getText().toString().trim());
        jsonObject.addProperty("password",password.getText().toString().trim());


        API api = RestAdapter2.createAPI();
        Call<responseRegister> call = api.postRegister(jsonObject);

        call.enqueue(new Callback<responseRegister>() {
            @Override
            public void onResponse(Call<responseRegister> call, Response<responseRegister> response) {
                responseRegister resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Gson gson = new Gson();
                    String json = gson.toJson(resp.getAkunConfig());

                    Log.e(getClass().getName(), "BERHASIL REGISTER "+json);

                    Toast.makeText(getApplicationContext(), "BERHASIL REGISTER",
                            Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(ActivityRegister.this, ActivityLogin.class);
                    startActivity(intent);

                } else {
                    //onFailRequest(resp.getErrorMsg());
                    Toast.makeText(getApplicationContext(), resp.getErrorMsg(),
                            Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<responseRegister> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });
    }

    private void onFailRequest(String msg) {
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true,msg);
        } else {
            showFailedView(true,getString(R.string.no_internet_text));
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
    private boolean validateName() {
        String str = name.getText().toString().trim();
        if (str.isEmpty()) {
            name_lyt.setError(getString(R.string.invalid_name));
            requestFocus(name);
            return false;
        } else {
            name_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateEmail() {
        String str = email.getText().toString().trim();
        if (str.isEmpty() || !Tools.isValidEmail(str)) {
            email_lyt.setError(getString(R.string.invalid_email));
            requestFocus(email);
            return false;
        } else {
            email_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateUserName() {
        String str = username.getText().toString().trim();
        if (str.isEmpty()) {
            buyer_name_lyt.setError(getString(R.string.invalid_username));
            requestFocus(username);
            return false;
        } else {
            buyer_name_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        String str = password.getText().toString().trim();
        if (str.isEmpty()) {
            password_lyt.setError(getString(R.string.invalid_name));
            requestFocus(password);
            return false;
        } else {
            password_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
