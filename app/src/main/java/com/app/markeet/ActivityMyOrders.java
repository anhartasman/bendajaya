package com.app.markeet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Transaction.list.Order;
import com.app.markeet.model.Transaction.list.Transactions;
import com.app.markeet.model.Transaction.response.responseConfirmTransaction;
import com.app.markeet.model.Transaction.response.responseDetailTransaction;
import com.app.markeet.model.Transaction.response.responseMyOrders;
import com.app.markeet.model.Wallet.detail.TopUpDetail;
import com.app.markeet.model.Wallet.response.responseConfirmTopUp;
import com.app.markeet.model.Wallet.response.responseRequestTopUp;
import com.app.markeet.model.Wallet.response.responseTopUpDetail;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import needle.Needle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class ActivityMyOrders extends AppCompatActivity {

    //deklarasi variabel
    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";
    TextView order_status=null;
    TextView order_list=null;
    TextView buyer_name=null;
    TextView buyer_phone=null;
    TextView tgl_trx=null;
    EditText seller_comment=null;
    TextInputLayout seller_comment_lyt=null;
    android.support.design.widget.AppBarLayout appbar_layout=null;
    android.support.v4.widget.NestedScrollView nested_content=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_process_order=null;
    TextView seller_comment_view,buyer_comment_view,total_price=null;
    LinearLayout lytbtnimage,seller_comment_view_lyt,buyer_comment_view_lyt,proses_status_lyt=null;
    Spinner spiner_order_status=null;

    ProgressDialog progressDialog = null;
    ThisApplication ta;
    static final int SET_CATEGORIES=192;
    LayoutInflater layoutInflater;
    android.app.AlertDialog ad=null;
    View dialogView=null;
    String product_id="";
    String notrx="";
    int status_selected=0;
    // activity transition
    public static void navigate(Activity activity,int transaksi_id,String notrx,int numresult,boolean from_notif) {
        Intent i = navigateBase(activity,transaksi_id,notrx,from_notif);
        //activity.startActivity(i);
        activity.startActivityForResult(i,numresult);
    }

    public static Intent navigateBase(Context context,int transaksi_id,String notrx,boolean from_notif) {
        Intent i = new Intent(context, ActivityMyOrders.class);
        i.putExtra("transaksi_id",transaksi_id);
        i.putExtra("notrx",notrx);
        i.putExtra("EXTRA_FROM_NOTIF",from_notif);

        return i;
    }

    private Long news_id;
    private Boolean from_notif;

    // extra obj
    private Transactions trxDetail;
    private List<Order> list_order;
    private static final int REQUEST_CHOOSER = 1234;

    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private WebView webview;
    int adarubah=0;
    int serverResponseCode=0;
    String alamatfile="";
    String img_str;
    Integer transaksi_id=0;
    Bitmap fotolama=null;
    List<String> status_list = new ArrayList<>();
    int[] status_list_value={0,1,2,3};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_detail);
        ta = ((ThisApplication)getApplicationContext());

        transaksi_id = getIntent().getIntExtra("transaksi_id", 0);
        notrx = getIntent().getStringExtra("notrx");
        from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);

        if(ta.list_categories!=null) {
            for (int i = 0; i < ta.list_categories.size(); i++) {
                ta.list_categories.get(i).setSelected(false);
            }
        }

        initComponent();
        initToolbar();
        requestAction();

        //akhir onCreate
    }

    private void initComponent() {
        parent_view = findViewById(android.R.id.content);


        lytbtnimage = (LinearLayout) findViewById(R.id.lytbtnimage);
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dialogView = layoutInflater.inflate(R.layout.dialog_setfoto, null);
        spiner_order_status = (Spinner) findViewById(R.id.spiner_order_status);

        order_status= (TextView)findViewById(R.id.order_status);
        buyer_name= (TextView)findViewById(R.id.buyer_name);
        buyer_phone= (TextView)findViewById(R.id.buyer_phone);
        tgl_trx= (TextView)findViewById(R.id.tgl_trx);
        order_list= (TextView)findViewById(R.id.order_list);
        buyer_comment_view= (TextView)findViewById(R.id.buyer_comment_view);
        seller_comment_view= (TextView)findViewById(R.id.seller_comment_view);
        seller_comment_lyt= (TextInputLayout)findViewById(R.id.seller_comment_lyt);
        seller_comment_view_lyt= (LinearLayout)findViewById(R.id.seller_comment_view_lyt);
        buyer_comment_view_lyt= (LinearLayout)findViewById(R.id.buyer_comment_view_lyt);
        proses_status_lyt= (LinearLayout)findViewById(R.id.proses_status_lyt);
        total_price= (TextView)findViewById(R.id.total_price);
        seller_comment= (EditText)findViewById(R.id.seller_comment);

        progressDialog = new ProgressDialog(ActivityMyOrders.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_getting_data));

        appbar_layout= (android.support.design.widget.AppBarLayout)findViewById(R.id.appbar_layout);
        nested_content= (android.support.v4.widget.NestedScrollView)findViewById(R.id.nested_content);
        lyt_process_order= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_process_order);

        lyt_process_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        final Button button = (Button) dialogView.findViewById(R.id.tombolattach);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                // Perform action on click
                File mPath = new File(Environment.getExternalStorageDirectory() + "//DIR//");
                FileDialog fileDialog = new FileDialog(ActivityMyOrders.this, mPath, ".txt");
                fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                    public void fileSelected(File file) {
                        Log.d(getClass().getName(), "selected file " + file.toString());
                        alamatfile=file.toString();

                        String filename = alamatfile;
                        String filenameArray[] = filename.split("\\.");
                        String extension = filenameArray[ filenameArray.length-1];
                        System.out.println(extension);
                        Log.d(getClass().getName(), "extensi file " + extension);


                        int size = (int) file.length();
                        byte[] bytes = new byte[size];
                        try {
                            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                            buf.read(bytes, 0, bytes.length);
                            buf.close();
                        } catch (FileNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        System.out.println("byte array:" + bytes);

                        img_str = Base64.encodeToString(bytes, 0);


                    }
                });

                if (Build.VERSION.SDK_INT < 19) {
                    fileDialog.showDialog();
                }else {

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    startActivityForResult(intent, REQUEST_CHOOSER);
                }
            }
        });
        final Button tombolcapture = (Button) dialogView.findViewById(R.id.tombolcapture);

        tombolcapture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File file = new File(Environment.getExternalStorageDirectory()+ File.separator + "image.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent, 1);

            }
        });
        requestAction();

        // Initialize and set Adapter

        //akhir initComponent
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Trx No #"+notrx);
    }

    private void requestAction() {
        progressDialog.setMessage(getString(R.string.content_getting_data));
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestDetailTransaction();
            }
        }, 1000);
    }

    private void onFailRequest(String msg) {
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, msg);
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void submitForm() {
        int lanjutsubmit=1;

        if(lanjutsubmit==1) {
            // hide keyboard
            hideKeyboard();

            // show dialog confirmation
            dialogConfirmSubmitForm();
        }
    }

    private void requestDetailTransaction() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","transaction");
        jsonObject.addProperty("action","get_trx_detail");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("transaction_id",transaksi_id);

        API api = RestAdapter2.createAPI();
        Call<responseDetailTransaction> callbackCall = api.getMyDetailTransaction(jsonObject);
        callbackCall.enqueue(new Callback<responseDetailTransaction>() {
            @Override
            public void onResponse(Call<responseDetailTransaction> call, Response<responseDetailTransaction> response) {
                final responseDetailTransaction resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equalsIgnoreCase("000")) {
                    trxDetail = resp.getResponData();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            requestDetailOrder();
                        }
                    });
                } else {
                    onFailRequest(resp.getErrorMsg());
                }
            }

            @Override
            public void onFailure(Call<responseDetailTransaction> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });

    }

    private void requestDetailOrder() {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","transaction");
        jsonObject.addProperty("action","get_my_orders");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("transaction_id",transaksi_id);

        API api = RestAdapter2.createAPI();
        Call<responseMyOrders> callbackCall = api.getMyOrders(jsonObject);
        callbackCall.enqueue(new Callback<responseMyOrders>() {
            @Override
            public void onResponse(Call<responseMyOrders> call, Response<responseMyOrders> response) {
                final responseMyOrders resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equalsIgnoreCase("000")) {
                    list_order = resp.getResponData();
                    runOnUiThread(new Runnable() {
                        public void run() {
                            displayDetailTransaction();
                        }
                    });
                } else {
                    onFailRequest(resp.getErrorMsg());
                }
            }

            @Override
            public void onFailure(Call<responseMyOrders> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });

    }

    private void displayDetailTransaction() {
        order_status.setText(Tools.getTrxStatus(trxDetail.getStatus()));
        buyer_name.setText(trxDetail.getRecipient_name());
        buyer_phone.setText(trxDetail.getRecipient_phone());
        tgl_trx.setText(trxDetail.getCreateAt());
        if(!trxDetail.getSellerComment().isEmpty()){
            seller_comment_view_lyt.setVisibility(View.VISIBLE);
            seller_comment_view.setText(trxDetail.getSellerComment());
        }
        if(!trxDetail.getComment().isEmpty()){
            buyer_comment_view_lyt.setVisibility(View.VISIBLE);
            buyer_comment_view.setText(trxDetail.getComment());
        }
        String dafbelanja="";
        Integer totalproductprice=0;
        for(int i=0;i <trxDetail.getOrders().size();i++){
            Integer itemprice=trxDetail.getOrders().get(i).getPriceItem();
            int itemamount=trxDetail.getOrders().get(i).getAmount();
            String itemname=trxDetail.getOrders().get(i).getProductName();
            Integer totalprice=(itemamount*itemprice);
            totalproductprice+=totalprice;
            dafbelanja+=String.valueOf(i+1)+". "+itemname+" ("+itemamount+" item) = "+Tools.getFormattedPrice(Double.valueOf(totalprice), this)+"\n\n";
        }
        order_list.setText(dafbelanja);
        total_price.setText(Tools.getFormattedPrice(Double.valueOf(totalproductprice), this));
        if(trxDetail.getStatus()>=2){
            lyt_process_order.setVisibility(GONE);
        }
        if(trxDetail.getStatus()<2){
            proses_status_lyt.setVisibility(View.VISIBLE);
            seller_comment_lyt.setVisibility(View.VISIBLE);
        }else{
            proses_status_lyt.setVisibility(View.GONE);
            seller_comment_lyt.setVisibility(View.GONE);
        }

        int[] statuslistbaru = status_list_value;
        Log.d(getClass().getName(),"ACCOUNT ID : "+String.valueOf(ta.akunConfig.getId()));
        if(trxDetail.getIds()==ta.akunConfig.getId()) {
            Log.d(getClass().getName(),"TERPOTONG!!!");
            if(trxDetail.getStatus()<2) {
                statuslistbaru = new int[]{0, 1, 3};
            }else{
                statuslistbaru = new int[]{trxDetail.getStatus()};
            }
        }else if(trxDetail.getIdb()==ta.akunConfig.getId()) {

            if(trxDetail.getStatus()<2) {
                statuslistbaru = new int[]{0,2};
            }else{
                statuslistbaru = new int[]{trxDetail.getStatus()};
            }

        }

        status_list.clear();
        for (int i = 0; i < statuslistbaru.length; i++) {
            status_list.add(Tools.getTrxStatus(statuslistbaru[i]));
        }
        final int[] slb=statuslistbaru;
        ArrayAdapter adapter_shipping = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, status_list);
        adapter_shipping.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner_order_status.setAdapter(adapter_shipping);

        spiner_order_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                status_selected=slb[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        int nomstatus=0;

        for(int i=0; i<statuslistbaru.length; i++){
            if(statuslistbaru[i]==trxDetail.getStatus()){
                nomstatus=i;
                break;
            }
        }

        spiner_order_status.setSelection(nomstatus);


    }
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean validateEmpty(EditText et, TextInputLayout ilayout, String err_msg) {
        String str = et.getText().toString().trim();
        if (str.isEmpty()) {
            ilayout.setError(err_msg);
            requestFocus(et);
            return false;
        } else {
            ilayout.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webview != null) webview.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webview != null) webview.onPause();
    }

    public void dialogConfirmSubmitForm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirmation);
        builder.setMessage(getString(R.string.confirm_submit_data));
        builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitFormTransaksi();
            }
        });
        builder.setNegativeButton(R.string.NO, null);
        builder.show();
    }

    private void delaySubmitFormTransaksi() {
        progressDialog.setMessage(getString(R.string.content_submit_topup));
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SubmitFormTransaksi();
            }
        }, 2000);
    }


    private void SubmitFormTransaksi() {
        // prepare insert form data
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","transaction");
        jsonObject.addProperty("action","confirm_transaction");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("transaction_id",transaksi_id);
        jsonObject.addProperty("status",status_selected);
        if(trxDetail.getIds()==ta.akunConfig.getId()) {
            jsonObject.addProperty("seller_comment", seller_comment.getText().toString());
        }else if(trxDetail.getIdb()==ta.akunConfig.getId()) {
            jsonObject.addProperty("buyer_comment", seller_comment.getText().toString());
        }

        API api = RestAdapter2.createAPI();
        Call<responseConfirmTransaction> call = api.confirmTransaction(jsonObject);

        call.enqueue(new Callback<responseConfirmTransaction>() {
            @Override
            public void onResponse(Call<responseConfirmTransaction> call, Response<responseConfirmTransaction> response) {
                responseConfirmTransaction resp = response.body();

                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Toast.makeText(ActivityMyOrders.this, "Success",
                            Toast.LENGTH_SHORT).show();
                    adarubah=1;

                        runOnUiThread(new Runnable() {
                            public void run() {
                                requestAction();
                            }
                        });

                } else {
                    Toast.makeText(ActivityMyOrders.this, "Failed",
                            Toast.LENGTH_SHORT).show();
                    onFailRequest(resp.getErrorMsg());
                }

            }

            @Override
            public void onFailure(Call<responseConfirmTransaction> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });


        //akhir submitFormData
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle b = new Bundle();
        b.putInt("adarubah", adarubah);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            onBackAction();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putInt("adarubah", adarubah);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
        onBackAction();
    }

    private void onBackAction() {
        if (from_notif) {
            if (ActivityMain.active) {
                finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), ActivitySplash.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

        }
    }


}
