package com.app.markeet.model;

import java.io.Serializable;

public class BuyerProfile implements Serializable {

    public String name;
    public String phone;
    public String address;
    public String address_id;
}
