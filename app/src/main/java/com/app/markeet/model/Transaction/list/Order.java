package com.app.markeet.model.Transaction.list;

import com.app.markeet.model.Products.list.Product;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("price_item")
    @Expose
    private Integer priceItem;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("idtrx")
    @Expose
    private String idtrx;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_at")
    @Expose
    private String createAt;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;
    @SerializedName("product_detail")
    @Expose
    private Product product_detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getPriceItem() {
        return priceItem;
    }

    public void setPriceItem(Integer priceItem) {
        this.priceItem = priceItem;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getIdtrx() {
        return idtrx;
    }

    public void setIdtrx(String idtrx) {
        this.idtrx = idtrx;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Product getProduct_detail() {
        return product_detail;
    }

    public void setProduct_detail(Product product_detail) {
        this.product_detail = product_detail;
    }
}
