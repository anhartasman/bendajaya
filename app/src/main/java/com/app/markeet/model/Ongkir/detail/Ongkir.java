package com.app.markeet.model.Ongkir.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ongkir {

    @SerializedName("ongkir")
    @Expose
    private String ongkir;

    public String getOngkir() {
        return ongkir;
    }

    public void setOngkir(String tax) {
        this.ongkir = tax;
    }

}
