package com.app.markeet.model.Menu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MenuItemImg implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("icon")
    @Expose
    private String icon;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public MenuItemImg(Integer id, String category, String info, String color){
        this.id=id;
        this.category=category;
        this.info=info;
        this.color=color;
        this.icon="file:///android_asset/aset/"+"ikontombol1.jpg";
    }

    public MenuItemImg(Integer id, String category, String info, String color, String icon){
        this.id=id;
        this.category=category;
        this.info=info;
        this.color=color;
        this.icon=icon;
    }

}
