package com.app.markeet.model;

import java.io.Serializable;

public class ProductOrder implements Serializable {

    public String buyer;
    public String address_id;
    public String shipping;
    public Long date_ship;
    public String phone;
    public String comment;
    public String status;
    public Double total_fees;
    public Double tax;
    public String serial;
    public Long created_at = System.currentTimeMillis();
    public Long last_update = System.currentTimeMillis();
    public Integer transport_service_id;
    public ProductOrder() {
    }

    public ProductOrder(BuyerProfile buyerProfile, String shipping, Long date_ship, String comment) {
        this.buyer = buyerProfile.name;
        this.address_id = buyerProfile.address_id;
        this.phone = buyerProfile.phone;
        this.shipping = shipping;
        this.date_ship = date_ship;
        this.comment = comment;
    }
}
