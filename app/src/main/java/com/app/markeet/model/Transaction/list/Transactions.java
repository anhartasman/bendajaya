package com.app.markeet.model.Transaction.list;

import com.app.markeet.model.Profile.view.Profile;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Transactions {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("notrx")
    @Expose
    private Integer notrx;
    @SerializedName("nogroup")
    @Expose
    private Integer nogroup;
    @SerializedName("idb")
    @Expose
    private Integer idb;
    @SerializedName("ids")
    @Expose
    private Integer ids;
    @SerializedName("idts")
    @Expose
    private Integer idts;
    @SerializedName("ida")
    @Expose
    private Integer ida;
    @SerializedName("idr")
    @Expose
    private Integer idr;
    @SerializedName("bill")
    @Expose
    private Integer bill;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("seller_comment")
    @Expose
    private String sellerComment;
    @SerializedName("admin_comment")
    @Expose
    private String adminComment;
    @SerializedName("create_at")
    @Expose
    private String createAt;
    @SerializedName("last_update")
    @Expose
    private String lastUpdate;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("orders")
    @Expose
    private List<Order> orders;
    @SerializedName("buyer_profile")
    @Expose
    private Profile buyer_profile;
    @SerializedName("seller_profile")
    @Expose
    private Profile seller_profile;
    @SerializedName("recipient_name")
    @Expose
    private String recipient_name;
    @SerializedName("recipient_phone")
    @Expose
    private String recipient_phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNotrx() {
        return notrx;
    }

    public void setNotrx(Integer notrx) {
        this.notrx = notrx;
    }

    public Integer getNogroup() {
        return nogroup;
    }

    public void setNogroup(Integer nogroup) {
        this.nogroup = nogroup;
    }

    public Integer getIdb() {
        return idb;
    }

    public void setIdb(Integer idb) {
        this.idb = idb;
    }

    public Integer getIds() {
        return ids;
    }

    public void setIds(Integer ids) {
        this.ids = ids;
    }

    public Integer getIdts() {
        return idts;
    }

    public void setIdts(Integer idts) {
        this.idts = idts;
    }

    public Integer getIda() {
        return ida;
    }

    public void setIda(Integer ida) {
        this.ida = ida;
    }

    public Integer getIdr() {
        return idr;
    }

    public void setIdr(Integer idr) {
        this.idr = idr;
    }

    public Integer getBill() {
        return bill;
    }

    public void setBill(Integer bill) {
        this.bill = bill;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSellerComment() {
        return sellerComment;
    }

    public void setSellerComment(String sellerComment) {
        this.sellerComment = sellerComment;
    }

    public String getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Profile getBuyer_profile() {
        return buyer_profile;
    }

    public void setBuyer_profile(Profile buyer_profile) {
        this.buyer_profile = buyer_profile;
    }

    public Profile getSeller_profile() {
        return seller_profile;
    }

    public void setSeller_profile(Profile seller_profile) {
        this.seller_profile = seller_profile;
    }

    public String getRecipient_name() {
        return recipient_name;
    }

    public void setRecipient_name(String recipient_name) {
        this.recipient_name = recipient_name;
    }

    public String getRecipient_phone() {
        return recipient_phone;
    }

    public void setRecipient_phone(String recipient_phone) {
        this.recipient_phone = recipient_phone;
    }
}
