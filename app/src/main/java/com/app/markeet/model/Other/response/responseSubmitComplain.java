package com.app.markeet.model.Other.response;

import com.app.markeet.model.Products.list.ObjResponseAddProduct;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class responseSubmitComplain {
    @SerializedName("module")
    @Expose
    private String module;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("error_code")
    @Expose
    private String errorCode;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("respon_data")
    @Expose
    private String respon_data = null;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRespon_data() {
        return respon_data;
    }

    public void setRespon_data(String respon_data) {
        this.respon_data = respon_data;
    }
}
