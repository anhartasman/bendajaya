package com.app.markeet.model.Products.response;

import com.app.markeet.model.Products.list.ObjResponseAddProduct;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class responseAddProduct {
    @SerializedName("module")
    @Expose
    private String module;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("error_code")
    @Expose
    private String errorCode;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("respon_data")
    @Expose
    private ObjResponseAddProduct responData = null;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ObjResponseAddProduct getResponData() {
        return responData;
    }

    public void setResponData(ObjResponseAddProduct responData) {
        this.responData = responData;
    }


}
