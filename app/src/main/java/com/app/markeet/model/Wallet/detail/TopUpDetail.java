package com.app.markeet.model.Wallet.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopUpDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bank_name")
    @Expose
    private String bank_name;
    @SerializedName("account_name")
    @Expose
    private String account_name;
    @SerializedName("account_number")
    @Expose
    private String account_number;
    @SerializedName("topup_amount")
    @Expose
    private String topup_amount;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_at")
    @Expose
    private String create_at;
    @SerializedName("buktitransfer")
    @Expose
    private String buktitransfer;
    @SerializedName("user_comment")
    @Expose
    private String user_comment;
    @SerializedName("admin_comment")
    @Expose
    private String admin_comment;
    @SerializedName("expired_date")
    @Expose
    private String expired_date;

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getTopup_amount() {
        return topup_amount;
    }

    public void setTopup_amount(String topup_amount) {
        this.topup_amount = topup_amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBuktitransfer() {
        return buktitransfer;
    }

    public void setBuktitransfer(String buktitransfer) {
        this.buktitransfer = buktitransfer;
    }

    public String getUser_comment() {
        return user_comment;
    }

    public void setUser_comment(String user_comment) {
        this.user_comment = user_comment;
    }

    public String getAdmin_comment() {
        return admin_comment;
    }

    public void setAdmin_comment(String admin_comment) {
        this.admin_comment = admin_comment;
    }

    public String getExpired_date() {
        return expired_date;
    }

    public void setExpired_date(String expired_date) {
        this.expired_date = expired_date;
    }
}
