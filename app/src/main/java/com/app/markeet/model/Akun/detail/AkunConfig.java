package com.app.markeet.model.Akun.detail;

import com.app.markeet.model.Akun.detail.AkunConfig;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AkunConfig {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sessid")
    @Expose
    private String sessid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("rolehost")
    @Expose
    private String rolehost;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("ecash")
    @Expose
    private String ecash;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSessid() {
        return sessid;
    }

    public void setSessid(String sessid) {
        this.sessid = sessid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRolehost() {
        return rolehost;
    }

    public void setRolehost(String rolehost) {
        this.rolehost = rolehost;
    }

    public String getECash() {
        return ecash;
    }

    public void setECash(String ecash) {
        this.ecash = ecash;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEcash() {
        return ecash;
    }

    public void setEcash(String ecash) {
        this.ecash = ecash;
    }
}
