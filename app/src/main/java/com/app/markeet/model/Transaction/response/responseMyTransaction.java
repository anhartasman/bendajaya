package com.app.markeet.model.Transaction.response;

import com.app.markeet.model.Transaction.list.Order;
import com.app.markeet.model.Transaction.list.Transactions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class responseMyTransaction {

    @SerializedName("module")
    @Expose
    private String module;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("error_code")
    @Expose
    private String errorCode;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("respon_data")
    @Expose
    private List<Transactions> responData = null;

    public List<Transactions> getResponData() {
        return responData;
    }

    public void setResponData(List<Transactions> responData) {
        this.responData = responData;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
