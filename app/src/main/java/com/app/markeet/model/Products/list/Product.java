package com.app.markeet.model.Products.list;


import com.app.markeet.model.ProductCategory.list.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Product implements Serializable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("ids")
    @Expose
    private Integer ids;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("mainpict")
    @Expose
    private String mainpict;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("last_update")
    @Expose
    private String lastupdate;
    @SerializedName("seller_name")
    @Expose
    private String seller_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIds() {
        return ids;
    }

    public void setIds(Integer ids) {
        this.ids = ids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getMainpict() {
        return mainpict;
    }

    public void setMainpict(String mainpict) {
        this.mainpict = mainpict;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(String lastupdate) {
        this.lastupdate = lastupdate;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }
}
