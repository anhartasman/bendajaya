package com.app.markeet.model.Products.post;

import com.app.markeet.model.ProductCategory.list.Category;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class post_add_product implements Serializable {

    public List<Category> product_categories = new ArrayList<>();
    public String module="product";
    public String action="add_product";
    public String sessid="";
    public String product_id="";
    public String product_name="";
    public String product_price="";
    public String product_description="";
}
