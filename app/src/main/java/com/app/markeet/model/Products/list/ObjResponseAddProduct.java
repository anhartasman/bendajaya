package com.app.markeet.model.Products.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjResponseAddProduct {
    @SerializedName("product_id")
    @Expose
    private String product_id;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
