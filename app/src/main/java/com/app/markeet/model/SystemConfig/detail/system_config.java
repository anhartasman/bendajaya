package com.app.markeet.model.SystemConfig.detail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class system_config {

    @SerializedName("tax")
    @Expose
    private String tax;

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

}
