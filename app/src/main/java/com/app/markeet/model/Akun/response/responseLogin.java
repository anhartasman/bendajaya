package com.app.markeet.model.Akun.response;

import com.app.markeet.model.Akun.detail.AkunConfig;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class responseLogin {
    @SerializedName("module")
    @Expose
    private String module;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("error_code")
    @Expose
    private String errorCode;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("jumtabel")
    @Expose
    private Integer jumtabel;
    @SerializedName("respon_data")
    @Expose
    private AkunConfig responData;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getJumtabel() {
        return jumtabel;
    }

    public void setJumtabel(Integer jumtabel) {
        this.jumtabel = jumtabel;
    }

    public AkunConfig getAkunConfig() {
        return responData;
    }

    public void setAkunConfig(AkunConfig responData) {
        this.responData = responData;
    }

}
