package com.app.markeet.model;

import java.io.Serializable;
import java.util.List;

public class CheckOngkir implements Serializable {

    public String module="transaction";
    public String action="check_ongkir";
    public String sessid="";
    public Integer transport_service_id=0;
    public Integer destination=0;
    public List<Integer> aridp;
}
