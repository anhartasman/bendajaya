package com.app.markeet;

import android.util.Log;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

/**
 * Created by anhar on 05/05/17.
 */
public class MyAPI {
    Download download = new Download();
    JSONObject objekjson=null;
    String stringobjek="";
    String error_code="";
    String error_msg="";
    JSONObject respon_data=null;
    JSONArray respon_dataarray=null;
    String respon_datastring="";
    JSONObject hasilambil=null;
    String hasilteks="";
    String urlnya="";
    DateTime waktureq=null;
    public MyAPI(Hashtable a, String u, String namaapi){
        waktureq=new DateTime();
        urlnya=u;
        objekjson = new JSONObject(a);
        stringobjek=objekjson.toString();
        //Log.d("aa","waktu kiriman : "+waktureq);
        Log.d(getClass().getName(),"kiriman "+namaapi+": "+waktureq+" "+stringobjek);
        hasilteks = download.kirimjson(stringobjek,urlnya);
        Log.d(getClass().getName(),"hasilteks "+namaapi+": "+hasilteks);

        try {

            if(hasilteks.startsWith("{")){
                hasilambil = new JSONObject(hasilteks);
                error_code = hasilambil.getString("error_code");
                error_msg = hasilambil.getString("error_msg");
                Log.d("a", "error_code "+error_code);
                if(error_code.equalsIgnoreCase("000")){


                }
            }else if(hasilteks.startsWith("[")){
              //  respon_dataarray=new JSONArray(tulisanrespon);
            }else{
                error_code = "001";
                error_msg = "terdapat error";
            }

        } catch (JSONException e) {

            e.printStackTrace();

        }
    }

    public String get_error_code(){
        return error_code;
    }
    public String get_error_msg(){
        return error_msg;
    }
    public JSONObject get_respon_data(){
        return respon_data;
    }
    public JSONObject get_hasilambil(){
        return hasilambil;
    }
    public String get_hasilteks(){
        return hasilteks;
    }
    public String get_respon_data_string(){
        return respon_datastring;
    }
    public JSONArray get_respon_dataarray(){
        return respon_dataarray;
    }
    public String get_isi_string(String key){
        String hasil="";
        try {
            hasil=hasilambil.getString(key);
        } catch (JSONException e) {

            e.printStackTrace();

        }
        return hasil;
    }
}
