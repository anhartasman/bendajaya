package com.app.markeet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Akun.response.responseLogin;
import com.app.markeet.model.Akun.response.responseUpdateAkun;
import com.app.markeet.utils.CallbackDialog;
import com.app.markeet.utils.DialogUtils;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySettingAccount extends AppCompatActivity {

    private View parent_view;
    private ActionBar actionBar;

    android.support.design.widget.TextInputLayout account_username_lyt=null;
    EditText account_username=null;
    android.support.design.widget.TextInputLayout account_name_lyt=null;
    EditText account_name=null;
    android.support.design.widget.TextInputLayout email_lyt=null;
    EditText email=null;
    android.support.design.widget.TextInputLayout phone_lyt=null;
    EditText phone=null;
    android.support.design.widget.TextInputLayout address_lyt=null;
    EditText address=null;
    android.support.design.widget.TextInputLayout password_lyt=null;
    EditText password=null;
    android.support.design.widget.TextInputLayout passwordupdate_lyt=null;
    EditText passwordupdate=null;
    android.support.design.widget.TextInputLayout confirmpasswordupdate_lyt=null;
    EditText confirmpasswordupdate=null;

    android.support.design.widget.AppBarLayout appbar_layout=null;
    android.support.v4.widget.NestedScrollView nested_content=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_submit_save=null;
    TextView tv_submit_save=null;

    ThisApplication ta;
    private Dialog dialog_failed = null;

    ProgressDialog progressDialog = null;
    ProgressDialog pdia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_account);
        parent_view = findViewById(android.R.id.content);
        ta = ((ThisApplication)getApplicationContext());

        account_username_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.account_username_lyt);
        account_username= (EditText)findViewById(R.id.account_username);
        account_name_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.account_name_lyt);
        account_name= (EditText)findViewById(R.id.account_name);
        email_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.email_lyt);
        email= (EditText)findViewById(R.id.email);
        phone_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.phone_lyt);
        phone= (EditText)findViewById(R.id.phone);
        address_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.address_lyt);
        address= (EditText)findViewById(R.id.address);
        password_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.password_lyt);
        password= (EditText)findViewById(R.id.password);
        passwordupdate_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.passwordupdate_lyt);
        passwordupdate= (EditText)findViewById(R.id.passwordupdate);
        confirmpasswordupdate_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.confirmpasswordupdate_lyt);
        confirmpasswordupdate= (EditText)findViewById(R.id.confirmpasswordupdate);

        appbar_layout= (android.support.design.widget.AppBarLayout)findViewById(R.id.appbar_layout);
        nested_content= (android.support.v4.widget.NestedScrollView)findViewById(R.id.nested_content);
        lyt_submit_save= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_submit_save);
        tv_submit_save= (TextView)findViewById(R.id.tv_submit_save);

        account_username.setText(ta.akunConfig.getUsername());
        account_name.setText(ta.akunConfig.getName());
        address.setText(ta.akunConfig.getAddress());
        phone.setText(ta.akunConfig.getPhone());
        password.setText("");
        email.setText(ta.akunConfig.getEmail());

        lyt_submit_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitSave();
            }
        });


        progressDialog = new ProgressDialog(ActivitySettingAccount.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_data));

        initToolbar();
        //akhir onCreate
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_account_settings);
        Tools.systemBarLolipop(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitSave() {
        int bisalogin=1;
        if (!validateUserName()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validateName()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validateEmail()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validateAddress()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validatePhone()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (! passwordupdate.getText().toString().equalsIgnoreCase(confirmpasswordupdate.getText().toString())) {
            Snackbar.make(parent_view, "Password baru tidak sama", Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if(passwordupdate.getText().toString().length()>0){
            if(password.getText().toString().length()==0){

                Snackbar.make(parent_view, "Ketik password lama untuk menerapkan password baru", Snackbar.LENGTH_SHORT).show();
                bisalogin=0;
                return;
            }
        }

        if(bisalogin==1){
            dialogConfirmSubmitForm();
        }


        // hide keyboard
        hideKeyboard();

    }

    public void dialogConfirmSubmitForm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirmation);
        builder.setMessage(getString(R.string.confirm_submit_data));
        builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitFormAkun();
            }
        });
        builder.setNegativeButton(R.string.NO, null);
        builder.show();
    }
    public void delaySubmitFormAkun(){

        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitFormAkun();
            }
        }, 2000);
    }
    public void submitFormAkun(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","update_account");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("username",account_username.getText().toString().trim());
        jsonObject.addProperty("name",account_name.getText().toString().trim());
        jsonObject.addProperty("email",email.getText().toString().trim());
        jsonObject.addProperty("phone",phone.getText().toString().trim());
        jsonObject.addProperty("address",address.getText().toString().trim());
        jsonObject.addProperty("password",password.getText().toString().trim());
        jsonObject.addProperty("confirmpasswordupdate",confirmpasswordupdate.getText().toString().trim());
        jsonObject.addProperty("passwordupdate",passwordupdate.getText().toString().trim());


        API api = RestAdapter2.createAPI();
        Call<responseUpdateAkun> call = api.postDataAkun(jsonObject);

        call.enqueue(new Callback<responseUpdateAkun>() {
            @Override
            public void onResponse(Call<responseUpdateAkun> call, Response<responseUpdateAkun> response) {
                responseUpdateAkun resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Gson gson = new Gson();
                    String json = gson.toJson(resp);

                    Log.e(getClass().getName(), "BERHASIL UPDATE ACCOUNT "+json);

                    Toast.makeText(getApplicationContext(), "Berhasil update account",Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getApplicationContext(), resp.getErrorMsg(),Toast.LENGTH_SHORT).show();

                    //onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<responseUpdateAkun> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });

    }
    private void onFailRequest() {
        if (NetworkCheck.isConnect(this)) {
            showDialogFailed(R.string.msg_failed_load_data);
        } else {
            showDialogFailed(R.string.no_internet_text);
        }
    }

    public void showDialogFailed(@StringRes int msg) {
        if (dialog_failed != null && dialog_failed.isShowing()) return;

        dialog_failed = new DialogUtils(this).buildDialogWarning(-1, msg, R.string.TRY_AGAIN, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog_failed.show();
    }

    // validation method
    private boolean validateEmail() {
        String str = email.getText().toString().trim();
        if (str.isEmpty() || !Tools.isValidEmail(str)) {
            email_lyt.setError(getString(R.string.invalid_email));
            requestFocus(email);
            return false;
        } else {
            email_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateUserName() {
        String str = account_username.getText().toString().trim();
        if (str.isEmpty()) {
            account_username_lyt.setError(getString(R.string.invalid_name));
            requestFocus(account_username);
            return false;
        } else {
            account_username_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateName() {
        String str = account_name.getText().toString().trim();
        if (str.isEmpty()) {
            account_name_lyt.setError(getString(R.string.invalid_name));
            requestFocus(account_name);
            return false;
        } else {
            account_name_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateAddress() {
        String str = address.getText().toString().trim();
        if (str.isEmpty()) {
            address_lyt.setError(getString(R.string.invalid_address));
            requestFocus(address);
            return false;
        } else {
            address_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePhone() {
        String str = phone.getText().toString().trim();
        if (str.isEmpty()) {
            phone_lyt.setError(getString(R.string.invalid_phone));
            requestFocus(phone);
            return false;
        } else {
            phone_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validatePassword() {
        String str = password.getText().toString().trim();
        if (str.isEmpty()) {
            password_lyt.setError(getString(R.string.invalid_password));
            requestFocus(password);
            return false;
        } else {
            password_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validatePasswordUpdate() {
        String str = passwordupdate.getText().toString().trim();
        if (str.isEmpty()) {
            passwordupdate_lyt.setError(getString(R.string.invalid_password));
            requestFocus(passwordupdate);
            return false;
        } else {
            passwordupdate_lyt.setErrorEnabled(false);
        }
        return true;
    }



    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
