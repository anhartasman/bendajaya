package com.app.markeet.connection;

import com.app.markeet.connection.callbacks.CallbackCategory;
import com.app.markeet.connection.callbacks.CallbackDevice;
import com.app.markeet.connection.callbacks.CallbackFeaturedNews;
import com.app.markeet.connection.callbacks.CallbackInfo;
import com.app.markeet.connection.callbacks.CallbackNewsInfo;
import com.app.markeet.connection.callbacks.CallbackNewsInfoDetails;
import com.app.markeet.connection.callbacks.CallbackOrder;
import com.app.markeet.connection.callbacks.CallbackProduct;
import com.app.markeet.connection.callbacks.CallbackProductDetails;
import com.app.markeet.data.Constant;
import com.app.markeet.model.Address.response.responseAddAddress;
import com.app.markeet.model.Address.response.responseGetAddress;
import com.app.markeet.model.Akun.response.responseGetAccount;
import com.app.markeet.model.Akun.response.responseLogin;
import com.app.markeet.model.Akun.response.responseRegister;
import com.app.markeet.model.Akun.response.responseUpdateAkun;
import com.app.markeet.model.CheckOngkir;
import com.app.markeet.model.Checkout;
import com.app.markeet.model.DeviceInfo;
import com.app.markeet.model.Ongkir.response.responseCheckOngkir;
import com.app.markeet.model.Other.response.responseSubmitComplain;
import com.app.markeet.model.Products.response.responseSearchProduct;
import com.app.markeet.model.Profile.response.responseGetProfile;
import com.app.markeet.model.Sellers.response.responseSearchSellerByCategory;
import com.app.markeet.model.Transaction.response.responseConfirmTransaction;
import com.app.markeet.model.Transaction.response.responseDetailTransaction;
import com.app.markeet.model.Transaction.response.responseMyOrders;
import com.app.markeet.model.Transaction.response.responseMyTransaction;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.model.Products.post.post_add_product;
import com.app.markeet.model.Products.response.get_product_detail;
import com.app.markeet.model.Products.response.get_products;
import com.app.markeet.model.Products.response.responseAddProduct;
import com.app.markeet.model.Products.response.responseDeleteProduct;
import com.app.markeet.model.Products.response.responseMyProducts;
import com.app.markeet.model.SystemConfig.response.responseGetSystemConfig;
import com.app.markeet.model.TransportService.response.responseGetTransportServices;
import com.app.markeet.model.Wallet.response.responseConfirmTopUp;
import com.app.markeet.model.Wallet.response.responseMyTopUps;
import com.app.markeet.model.Wallet.response.responseRequestTopUp;
import com.app.markeet.model.Wallet.response.responseTopUpDetail;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface API {

    String CACHE = "Cache-Control: max-age=0";
    String AGENT = "User-Agent: Markeet";
    String SECURITY = "Security: " + Constant.SECURITY_CODE;

    /* Recipe API transaction ------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/info")
    Call<CallbackInfo> getInfo(
            @Query("version") int version
    );

    /* Fcm API ----------------------------------------------------------- */
    @Headers({CACHE, AGENT, SECURITY})
    @POST("services/insertOneFcm")
    Call<CallbackDevice> registerDevice(
            @Body DeviceInfo deviceInfo
    );

    /* News Info API ---------------------------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/listFeaturedNews")
    Call<CallbackFeaturedNews> getFeaturedNews();

    @Headers({CACHE, AGENT})
    @GET("services/listNews")
    Call<CallbackNewsInfo> getListNewsInfo(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query
    );

    @Headers({CACHE, AGENT})
    @GET("services/getNewsDetails")
    Call<CallbackNewsInfoDetails> getNewsDetails(
            @Query("id") long id
    );

    /* Category API ---------------------------------------------------  */
    @Headers({CACHE, AGENT})
    @GET("services/listCategory")
    Call<CallbackCategory> getListCategory();


    /* Product API ---------------------------------------------------- */

    @Headers({CACHE, AGENT})
    @GET("services/listProduct")
    Call<CallbackProduct> getListProduct(
            @Query("page") int page,
            @Query("count") int count,
            @Query("q") String query,
            @Query("category_id") long category_id
    );

    @Headers({CACHE, AGENT})
    @GET("services/getProductDetails")
    Call<CallbackProductDetails> getProductDetails(
            @Query("id") long id
    );

    /* Checkout API ---------------------------------------------------- */
    @Headers({CACHE, AGENT, SECURITY})
    @POST("services/submitProductOrder")
    Call<CallbackOrder> submitProductOrder(
            @Body Checkout checkout
    );


    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<get_product_categories> getProductCategories(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<get_products> getProducts(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<get_product_detail> getProductDetail(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseLogin> postLogin(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseGetAccount> getAccount(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseGetAddress> getAddress(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseAddAddress> addAddress(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseGetSystemConfig> getSystemConfig(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseGetTransportServices> getTransportServices(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT, SECURITY})
    @POST("servis")
    Call<CallbackOrder> submitProductOrderToAPI(
            @Body Checkout checkout
    );

    @Headers({CACHE, AGENT, SECURITY})
    @POST("servis")
    Call<responseCheckOngkir> submitCheckOngkir(
            @Body CheckOngkir jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseAddProduct> addProduct(
            @Body post_add_product pap
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseMyProducts> myProducts(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseDeleteProduct> deleteProduct(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseRequestTopUp> requestTopUp(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseTopUpDetail> getTopUpDetail(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseConfirmTopUp> ConfirmTopUp(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseMyTopUps> getMyTopUps(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseMyTransaction> getMyTransactions(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseMyOrders> getMyOrders(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseDetailTransaction> getMyDetailTransaction(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseConfirmTransaction> confirmTransaction(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseRegister> postRegister(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseUpdateAkun> postDataAkun(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseSearchProduct> searchProduct(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseGetProfile> getProfile(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseSearchSellerByCategory> searchSeller(
            @Body JsonObject jsonObject
    );

    @Headers({CACHE, AGENT})
    @POST("servis")
    Call<responseSubmitComplain> submitComplain(
            @Body JsonObject jsonObject
    );



}
