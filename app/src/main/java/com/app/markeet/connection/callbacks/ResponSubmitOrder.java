package com.app.markeet.connection.callbacks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ResponSubmitOrder {

    @SerializedName("pesan")
    @Expose
    private String pesan;
    @SerializedName("notrx")
    @Expose
    private String notrx;
    @SerializedName("idtrx")
    @Expose
    private String idtrx;

    public String getPesan() {
        return pesan;
    }

    public String getNoTrx() {
        return notrx;
    }

    public void setNoTrx(String notrx) {
        this.notrx = notrx;
    }

    public String getIdTrx() {
        return idtrx;
    }

    public void setIdTrx(String trxid) {
        this.idtrx = trxid;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
