package com.app.markeet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.app.markeet.adapter.AdapterMyTransactions;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Menu.MenuItemImg;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.model.Transaction.list.Transactions;
import com.app.markeet.model.Transaction.response.responseMyTransaction;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Hashtable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyTransactions extends AppCompatActivity {

    // activity transition
    public static void navigate(Activity activity, int status, String bahantitle) {
        Intent i = new Intent(activity, ActivityMyTransactions.class);
        i.putExtra("status", status);
        i.putExtra("bahantitle", bahantitle);
        activity.startActivity(i);
    }

    private View parent_view;
    private RecyclerView recyclerView;
    private Call<get_product_categories> callbackCall;
    private AdapterMyTransactions adapter;
    Hashtable<Object, Object> tabelvalue = new Hashtable<Object, Object>();
    int TRANSACTION=23;
    ThisApplication ta;
    int status=0;
    String bahantitle="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shop);
        ta = ((ThisApplication)getApplicationContext());
        status = getIntent().getIntExtra("status", 0);
        bahantitle = getIntent().getStringExtra("bahantitle");
        parent_view = findViewById(android.R.id.content);
        initComponent();
        initToolbar();

        requestListOrder();

        recyclerView.setVisibility(View.VISIBLE);
    }


    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(bahantitle);
        Tools.systemBarLolipop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(ActivityMyTransactions.this, 1));

        //set data and list adapter
        adapter = new AdapterMyTransactions(ActivityMyTransactions.this, new ArrayList<Transactions>());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener(new AdapterMyTransactions.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Transactions obj, Integer posisi) {
                Snackbar.make(parent_view, obj.getNotrx().toString(), Snackbar.LENGTH_SHORT).show();
                ActivityMyOrders.navigate(ActivityMyTransactions.this, obj.getId(),obj.getNotrx().toString(),TRANSACTION,false);

            }
        });
    }

    private void requestListOrder() {

// Using the Retrofit
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","transaction");
        jsonObject.addProperty("action","get_my_transactions");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("status",status);


        API api = RestAdapter2.createAPI();
        Call<responseMyTransaction> call = api.getMyTransactions(jsonObject);

        call.enqueue(new Callback<responseMyTransaction>() {
            @Override
            public void onResponse(Call<responseMyTransaction> call, Response<responseMyTransaction> response) {
                responseMyTransaction resp = response.body();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.getResponData());

                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<responseMyTransaction> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(ActivityMyTransactions.this)) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TRANSACTION) {
            Bundle bundle = data.getExtras();

            //int adarubah = bundle.getInt("adarubah",0);
            //if(adarubah==1){
            requestListOrder();
            //}
        }
    }
}
