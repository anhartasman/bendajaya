package com.app.markeet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.connection.callbacks.CallbackNewsInfoDetails;
import com.app.markeet.model.NewsInfo;
import com.app.markeet.model.ProductCategory.list.Category;
import com.app.markeet.model.Products.post.post_add_product;
import com.app.markeet.model.Products.response.responseAddProduct;
import com.app.markeet.model.Wallet.response.responseRequestTopUp;
import com.app.markeet.utils.NetworkCheck;
import com.google.gson.JsonObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import needle.Needle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityRequestTopup extends AppCompatActivity {

    //deklarasi variabel
    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";
    android.support.design.widget.TextInputLayout bank_name_lyt=null;
    EditText bank_name=null;
    android.support.design.widget.TextInputLayout account_number_lyt=null;
    EditText account_number=null;
    android.support.design.widget.TextInputLayout account_name_lyt=null;
    EditText account_name=null;
    android.support.design.widget.TextInputLayout topup_amount_lyt=null;
    EditText topup_amount=null;
    android.support.design.widget.AppBarLayout appbar_layout=null;
    android.support.v4.widget.NestedScrollView nested_content=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_request_topup=null;
    TextView tv_requesttopup=null;

    Button btnChooseImgByGalery=null;
    SquareImageView fotoprofil=null;

    ProgressDialog progressDialog = null;
    ProgressDialog pdia;
    File filepilihan=null;
    ThisApplication ta;
    static final int SET_CATEGORIES=192;
    LayoutInflater layoutInflater;
    android.app.AlertDialog ad=null;
    View dialogView=null;
    Bitmap product_photo=null;
    String product_id="";
    int statusupload=0;
    // activity transition
    public static void navigate(Activity activity) {
        Intent i = navigateBase(activity);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context) {
        Intent i = new Intent(context, ActivityRequestTopup.class);

        return i;
    }

    private Long news_id;
    private Boolean from_notif;

    // extra obj
    private NewsInfo newsInfo;
    private static final int REQUEST_CHOOSER = 1234;

    private Call<CallbackNewsInfoDetails> callbackCall = null;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private WebView webview;
    int adarubah=0;
    int serverResponseCode=0;
    String alamatfile="";
    byte[] bytesnya;
    String img_str;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_topup);
        ta = ((ThisApplication)getApplicationContext());

        news_id = getIntent().getLongExtra(EXTRA_OBJECT_ID, -1L);
        from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);

        if(ta.list_categories!=null) {
            for (int i = 0; i < ta.list_categories.size(); i++) {
                ta.list_categories.get(i).setSelected(false);
            }
        }

        initComponent();
        initToolbar();
        requestAction();

        //akhir onCreate
    }

    private void initComponent() {
        parent_view = findViewById(android.R.id.content);

        List<String> list = new ArrayList<String>();
        list.add("List1");
        list.add("List2");

        fotoprofil = (SquareImageView) findViewById(R.id.imageProduct);
        layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dialogView = layoutInflater.inflate(R.layout.dialog_setfoto, null);
        btnChooseImgByGalery= (Button)findViewById(R.id.btnChooseImgByGalery);

        bank_name_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.bank_name_lyt);
        bank_name= (EditText)findViewById(R.id.bank_name);
        account_number_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.account_number_lyt);
        account_number= (EditText)findViewById(R.id.account_number);
        account_name_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.account_name_lyt);
        account_name= (EditText)findViewById(R.id.account_name);
        topup_amount_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.topup_amount_lyt);
        topup_amount= (EditText)findViewById(R.id.topup_amount);
        btnChooseImgByGalery= (Button)findViewById(R.id.btnChooseImgByGalery);


        progressDialog = new ProgressDialog(ActivityRequestTopup.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_topup));

        appbar_layout= (android.support.design.widget.AppBarLayout)findViewById(R.id.appbar_layout);
        nested_content= (android.support.v4.widget.NestedScrollView)findViewById(R.id.nested_content);
        lyt_request_topup= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_request_topup);
        tv_requesttopup= (TextView)findViewById(R.id.tv_requesttopup);


        lyt_request_topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        fotoprofil.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


            }
        });

        btnChooseImgByGalery.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ActivityRequestTopup.this, R.style.ThemeDialogCustom);
                builder.setTitle(R.string.upload_product_photo);
                //builder.setMessage("HALOO".replaceAll("<br>","\n"));


                builder.setView(dialogView);
                //builder.show();

                if(ad==null) {
                    ad = builder.show();
                }else {
                    ad.show();
                }

            }
        });
        final Button button = (Button) dialogView.findViewById(R.id.tombolattach);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                // Perform action on click
                File mPath = new File(Environment.getExternalStorageDirectory() + "//DIR//");
                FileDialog fileDialog = new FileDialog(ActivityRequestTopup.this, mPath, ".txt");
                fileDialog.addFileListener(new FileDialog.FileSelectedListener() {
                    public void fileSelected(File file) {
                        Log.d(getClass().getName(), "selected file " + file.toString());
                        alamatfile=file.toString();

                        String filename = alamatfile;
                        String filenameArray[] = filename.split("\\.");
                        String extension = filenameArray[ filenameArray.length-1];
                        System.out.println(extension);
                        Log.d(getClass().getName(), "extensi file " + extension);


                        int size = (int) file.length();
                        byte[] bytes = new byte[size];
                        try {
                            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                            buf.read(bytes, 0, bytes.length);
                            buf.close();
                        } catch (FileNotFoundException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        System.out.println("byte array:" + bytes);

                        img_str = Base64.encodeToString(bytes, 0);


                    }
                });

                if (Build.VERSION.SDK_INT < 19) {
                    fileDialog.showDialog();
                }else {

                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    startActivityForResult(intent, REQUEST_CHOOSER);
                }
            }
        });
        final Button tombolcapture = (Button) dialogView.findViewById(R.id.tombolcapture);

        tombolcapture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                File file = new File(Environment.getExternalStorageDirectory()+ File.separator + "image.jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intent, 1);

            }
        });
        //akhir initComponent
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_request_topup);
    }

    private void requestAction() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestNewsInfoDetailsApi();
            }
        }, 1000);
    }

    private void onFailRequest(String msg) {
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, msg);
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void submitForm() {
        int lanjutsubmit=1;
        if (!validateEmpty(bank_name,bank_name_lyt,getString(R.string.invalid_bank_name))) {
            Snackbar.make(parent_view, R.string.invalid_bank_name, Snackbar.LENGTH_SHORT).show();
            lanjutsubmit=0;
            return;
        }

        if (!validateEmpty(account_name,account_name_lyt,getString(R.string.invalid_account_name))) {
            Snackbar.make(parent_view, R.string.invalid_account_name, Snackbar.LENGTH_SHORT).show();
            lanjutsubmit=0;
            return;
        }

        if (!validateEmpty(account_number,account_number_lyt,getString(R.string.invalid_account_number))) {
            Snackbar.make(parent_view, R.string.invalid_account_number, Snackbar.LENGTH_SHORT).show();
            lanjutsubmit=0;
            return;
        }

        if (!validateEmpty(topup_amount,topup_amount_lyt,getString(R.string.invalid_topup_amount))) {
            Snackbar.make(parent_view, R.string.invalid_topup_amount, Snackbar.LENGTH_SHORT).show();
            lanjutsubmit=0;
            return;
        }


        if(lanjutsubmit==1) {
            // hide keyboard
            hideKeyboard();

            // show dialog confirmation
            dialogConfirmSubmitForm();
        }
    }

    private void requestNewsInfoDetailsApi() {
        /**
         API api = RestAdapter.createAPI();
         callbackCall = api.getNewsDetails(news_id);
         callbackCall.enqueue(new Callback<CallbackNewsInfoDetails>() {
        @Override
        public void onResponse(Call<CallbackNewsInfoDetails> call, Response<CallbackNewsInfoDetails> response) {
        CallbackNewsInfoDetails resp = response.body();
        if (resp != null && resp.status.equals("success")) {
        newsInfo = resp.news_info;
        displayPostData();
        } else {
        onFailRequest();
        }
        }

        @Override
        public void onFailure(Call<CallbackNewsInfoDetails> call, Throwable t) {
        Log.e("onFailure", t.getMessage());
        if (!call.isCanceled()) onFailRequest();
        }

        });
         **/
    }


    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean validateEmpty(EditText et, TextInputLayout ilayout, String err_msg) {
        String str = et.getText().toString().trim();
        if (str.isEmpty()) {
            ilayout.setError(err_msg);
            requestFocus(et);
            return false;
        } else {
            ilayout.setErrorEnabled(false);
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webview != null) webview.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webview != null) webview.onPause();
    }

    public void dialogConfirmSubmitForm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirmation);
        builder.setMessage(getString(R.string.confirm_submit_data));
        builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitFormProduk();
            }
        });
        builder.setNegativeButton(R.string.NO, null);
        builder.show();
    }

    private void delaySubmitFormProduk() {
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitFormProduk();
            }
        }, 2000);
    }

    private void delaySubmitGambarProduk() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitGambarProduk();
            }
        }, 2000);
    }

    private void submitFormProduk() {
        // prepare insert form data
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","wallet");
        jsonObject.addProperty("action","request_topup");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("bank_name",bank_name.getText().toString());
        jsonObject.addProperty("account_name",account_name.getText().toString());
        jsonObject.addProperty("account_number",account_number.getText().toString());
        jsonObject.addProperty("topup_amount",topup_amount.getText().toString());

        API api = RestAdapter2.createAPI();
        Call<responseRequestTopUp> call = api.requestTopUp(jsonObject);

        call.enqueue(new Callback<responseRequestTopUp>() {
            @Override
            public void onResponse(Call<responseRequestTopUp> call, Response<responseRequestTopUp> response) {
                responseRequestTopUp resp = response.body();
                progressDialog.dismiss();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    Toast.makeText(ActivityRequestTopup.this, "Success",
                            Toast.LENGTH_SHORT).show();
                    adarubah=1;

                    ActivityDetailTopup.navigate(ActivityRequestTopup.this,Integer.valueOf(resp.getResponData().getId()),false);
                    //onBackPressed();

                } else {
                    Toast.makeText(ActivityRequestTopup.this, "Failed",
                            Toast.LENGTH_SHORT).show();
                    onFailRequest(resp.getErrorMsg());
                }

            }

            @Override
            public void onFailure(Call<responseRequestTopUp> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });


        //akhir submitFormData
    }

    public void submitGambarProduk(){


        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                String ergebnis;
                try {
                 ergebnis = new postFotoProduk().execute().get();
                } catch (InterruptedException e) {
                    ergebnis = "Keine Daten";
                } catch (ExecutionException e) {
                    ergebnis = "Keine Daten";
                }
            }
        });
        //akhir submitGambarProduk
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle b = new Bundle();
        b.putInt("adarubah", adarubah);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            onBackAction();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putInt("adarubah", adarubah);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
        onBackAction();
    }

    private void onBackAction() {
        if (from_notif) {
            if (ActivityMain.active) {
                finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), ActivitySplash.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_CHOOSER:
                if (resultCode == Activity.RESULT_OK) {
                    //String alamatpilihan=getRealPathFromURI(data.getData(),getContext());
                    String alamatpilihan=getPath(ActivityRequestTopup.this,data.getData());
                    Log.d("a", "alamat " + alamatpilihan);
                    if(alamatpilihan!=null) {
                        File file = new File(alamatpilihan);
                        filepilihan=file;

                        if(filepilihan!=null) {
                            pdia = new ProgressDialog(ActivityRequestTopup.this);
                            pdia.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            pdia.setMessage("Uploading. Please wait");
                            pdia.setIndeterminate(true);
                            pdia.setCancelable(false);
                           // pdia.show();
                            gantiFotoProfil();

                        }

                    }

                    ad.dismiss();

                }

                break;
            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    //      Bitmap photo = (Bitmap) data.getExtras().get("data");


                    File file = new File(Environment.getExternalStorageDirectory()+File.separator + "image.jpg");
                    Bitmap bitmap = decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);

                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                    Uri tempUri = getImageUri(ActivityRequestTopup.this.getApplicationContext(), bitmap);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    filepilihan=file;

                    System.out.println(finalFile.toString());
                    if(filepilihan!=null) {
                        pdia = new ProgressDialog(ActivityRequestTopup.this);
                        pdia.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        pdia.setMessage("Uploading. Please wait");
                        pdia.setIndeterminate(true);
                        pdia.setCancelable(false);
                        //pdia.show();
                        gantiFotoProfil();
                    }
                    //storeCameraPhotoInSDCard(bitmap,"aaa");
                    ad.dismiss();
                }

                break;

        }
    }


    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
    { // BEST QUALITY MATCH

        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = ActivityRequestTopup.this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    private class postFotoProduk extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                String upLoadServerUri = ta.urlWeb+"kirimfotoproduk";
                String fileName = alamatfile;

                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                /**
                 File sourceFile = new File(alamatfile);
                 Log.d("a", "Source File "+alamatfile);
                 if (!sourceFile.isFile()) {
                 Log.e("uploadFile", "Source File Does not exist");
                 Log.d("a", "Source File Does not exist");
                 return "0";
                 }
                 **/
                try {
                    URL url = new URL(upLoadServerUri);
                    conn = (HttpURLConnection) url.openConnection();
                    // conn.setDoInput(true); // Allow Inputs
                    // conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=" + boundary);
                    conn.setRequestProperty("uploaded_file", fileName);
                    dos = new DataOutputStream(conn.getOutputStream());
                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    // untuk parameter sessid
                    dos.writeBytes("Content-Disposition: form-data; name=\"sessid\""
                            + lineEnd);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(String.valueOf(ta.akunConfig.getSessid()));
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    // untuk parameter id produk
                    dos.writeBytes("Content-Disposition: form-data; name=\"idp\""+lineEnd);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(product_id);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + lineEnd);

                    // jika ingin menambahkan parameter baru, silahkan buat baris baru
                    // lagi seperti berikut
                    // dos.writeBytes("Content-Disposition: form-data; name=\"keterangan\""+
                    // lineEnd);
                    // dos.writeBytes(lineEnd);
                    // dos.writeBytes(keterangan);
                    // dos.writeBytes(lineEnd);
                    // dos.writeBytes(twoHyphens + boundary + lineEnd);
                    FileInputStream fileInputStream = new FileInputStream(filepilihan);
                    dos.writeBytes("Content-Disposition: form-data; name=\"fotoproduk\";filename=\""
                            + filepilihan.toString() + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);
                    // create a buffer of maximum size
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    fileInputStream.close();


                    serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();

                    progressDialog.dismiss();
                    if (serverResponseCode == 200) {
                        ActivityRequestTopup.this.runOnUiThread(new Runnable() {
                            public void run() {
                                Log.d("a","BERHASIL");
                                Toast.makeText(ActivityRequestTopup.this, "Upload Berhasil.",
                                        Toast.LENGTH_SHORT).show();

                            }
                        });
                        statusupload=1;
                    }else{

                        ActivityRequestTopup.this.runOnUiThread(new Runnable() {
                            public void run() {
                                Log.d("a","GAGAL KIRIM "+serverResponseCode);
                                Toast.makeText(ActivityRequestTopup.this, "GAGAL UPLOAD FOTO PRODUK",
                                        Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                    onBackPressed();

                    // close the streams //
                    dos.flush();
                    dos.close();

                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                    Toast.makeText(ActivityRequestTopup.this, "MalformedURLException",
                            Toast.LENGTH_SHORT).show();
                    Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(ActivityRequestTopup.this, "Exception : " + e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                    // Log.e("Upload file to server Exception", "Exception : " + e.getMessage(), e);
                }
                return String.valueOf(statusupload);

            } catch (Exception e) {
                return null;
            }


        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            Log.d("a","async mulai");

        }

        @Override
        protected void onPostExecute(String result){
            super.onPostExecute(result);
            Log.d("a","async stop");
            //     pdia.dismiss();
        }
    }
    public void gantiFotoProfil(){
        ActivityRequestTopup.this.runOnUiThread(new Runnable() {
            public void run() {
                Uri uri = Uri.fromFile(filepilihan);
                /**
                 try {
                 Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                 activity.fotoprofil=bitmap;
                 fotoprofil.setImageDrawable(new RoundImage(bitmap));

                 }catch(IOException ioe){

                 }
                 */
                fotoprofil.setImageURI(uri);
                Bitmap bitmap = ((BitmapDrawable)fotoprofil.getDrawable()).getBitmap();
                Bitmap fotoprofilbmp=bitmap;
                Bitmap hasilkecil=getResizedBitmap(bitmap,300);
                product_photo=hasilkecil;
               // fotoprofil.setImageBitmap(bitmap);
                //fotoprofil.setImageDrawable(fotoprofil.getDrawable());
                //fotoprofil.setImageDrawable(new RoundImage(bitmap));
                fotoprofil.setImageBitmap(hasilkecil);

            }
        });
    }
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    public Drawable resizeImage(int imageResource) {// R.drawable.large_image
        // Get device dimensions
        Display display = getWindowManager().getDefaultDisplay();
        double deviceWidth = display.getWidth();

        BitmapDrawable bd = (BitmapDrawable) this.getResources().getDrawable(
                imageResource);
        double imageHeight = bd.getBitmap().getHeight();
        double imageWidth = bd.getBitmap().getWidth();

        double ratio = deviceWidth / imageWidth;
        int newImageHeight = (int) (imageHeight * ratio);

        Bitmap bMap = BitmapFactory.decodeResource(getResources(), imageResource);
        Drawable drawable = new BitmapDrawable(this.getResources(),
                getResizedBitmap2(bMap, newImageHeight, (int) deviceWidth));

        return drawable;
    }
    public Bitmap getResizedBitmap2(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();
        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }
}
