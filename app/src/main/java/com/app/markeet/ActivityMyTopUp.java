package com.app.markeet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.app.markeet.adapter.AdapterMyTopUp;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Menu.MenuItemImg;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.model.Wallet.detail.TopUpDetail;
import com.app.markeet.model.Wallet.response.responseMyTopUps;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Hashtable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyTopUp extends AppCompatActivity {

    // activity transition
    public static void navigate(Activity activity, int status,String bahantitle) {
        Intent i = new Intent(activity, ActivityMyTopUp.class);
        i.putExtra("status", status);
        i.putExtra("bahantitle", bahantitle);
        activity.startActivity(i);
    }

    private View parent_view;
    private RecyclerView recyclerView;
    private Call<get_product_categories> callbackCall;
    private AdapterMyTopUp adapter;
    Hashtable<Object, Object> tabelvalue = new Hashtable<Object, Object>();
    ArrayList<MenuItemImg> dafmenu =  new ArrayList<MenuItemImg>();
    int status=0;
    String bahantitle="";
    ThisApplication ta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        parent_view = findViewById(android.R.id.content);
        ta = ((ThisApplication)getApplicationContext());
        status = getIntent().getIntExtra("status", 0);
        bahantitle = getIntent().getStringExtra("bahantitle");
        initComponent();
        initToolbar();

        requestListTopUp();

        recyclerView.setVisibility(View.VISIBLE);
    }


    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(bahantitle);
        Tools.systemBarLolipop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(ActivityMyTopUp.this, 1));

        //set data and list adapter
        adapter = new AdapterMyTopUp(ActivityMyTopUp.this, new ArrayList<TopUpDetail>());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener(new AdapterMyTopUp.OnItemClickListener() {
            @Override
            public void onItemClick(View view, TopUpDetail obj, Integer posisi) {

                Snackbar.make(parent_view, obj.getTopup_amount(), Snackbar.LENGTH_SHORT).show();
                ActivityDetailTopup.navigate(ActivityMyTopUp.this, obj.getId(),false);

            }
        });
    }

    private void requestListTopUp() {

// Using the Retrofit
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","wallet");
        jsonObject.addProperty("action","get_my_topups");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("status",status);

        API api = RestAdapter2.createAPI();
        Call<responseMyTopUps> call = api.getMyTopUps(jsonObject);

        call.enqueue(new Callback<responseMyTopUps>() {
            @Override
            public void onResponse(Call<responseMyTopUps> call, Response<responseMyTopUps> response) {
                responseMyTopUps resp = response.body();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.getTopUps());

                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<responseMyTopUps> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(ActivityMyTopUp.this)) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

}
