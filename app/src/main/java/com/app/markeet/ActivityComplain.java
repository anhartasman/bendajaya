package com.app.markeet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Akun.response.responseUpdateAkun;
import com.app.markeet.model.Other.response.responseSubmitComplain;
import com.app.markeet.utils.CallbackDialog;
import com.app.markeet.utils.DialogUtils;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityComplain extends AppCompatActivity {

    private View parent_view;
    private ActionBar actionBar;

    android.support.design.widget.TextInputLayout message_body_lyt=null;
    EditText message_body=null;
    android.support.design.widget.TextInputLayout message_title_lyt=null;
    EditText message_title=null;


    android.support.design.widget.AppBarLayout appbar_layout=null;
    android.support.v4.widget.NestedScrollView nested_content=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_submit_save=null;
    TextView tv_submit_save=null;

    ThisApplication ta;
    private Dialog dialog_failed = null;

    ProgressDialog progressDialog = null;
    ProgressDialog pdia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain);
        parent_view = findViewById(android.R.id.content);
        ta = ((ThisApplication)getApplicationContext());

        lyt_submit_save= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_submit_save);
        message_title_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.message_title_lyt);
        message_title= (EditText)findViewById(R.id.message_title);
        message_body_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.message_body_lyt);
        message_body= (EditText)findViewById(R.id.message_body);


        lyt_submit_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitSave();
            }
        });


        progressDialog = new ProgressDialog(ActivityComplain.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_data));

        initToolbar();
        //akhir onCreate
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.form_keluhan);
        Tools.systemBarLolipop(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitSave() {
        int bisalogin=1;
        if (!validateJudulPesan()) {
            Snackbar.make(parent_view, R.string.invalid_message_title, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validateIsiPesan()) {
            Snackbar.make(parent_view, R.string.invalid_message_body, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }


        if(bisalogin==1){
            dialogConfirmSubmitForm();
        }


        // hide keyboard
        hideKeyboard();

    }

    public void dialogConfirmSubmitForm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirmation);
        builder.setMessage(getString(R.string.confirm_submit_data));
        builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitFormAkun();
            }
        });
        builder.setNegativeButton(R.string.NO, null);
        builder.show();
    }
    public void delaySubmitFormAkun(){

        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitFormAkun();
            }
        }, 2000);
    }
    public void submitFormAkun(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","submit_complain");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("message_body",message_body.getText().toString().trim());
        jsonObject.addProperty("message_title",message_title.getText().toString().trim());

        API api = RestAdapter2.createAPI();
        Call<responseSubmitComplain> call = api.submitComplain(jsonObject);

        call.enqueue(new Callback<responseSubmitComplain>() {
            @Override
            public void onResponse(Call<responseSubmitComplain> call, Response<responseSubmitComplain> response) {
                responseSubmitComplain resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Gson gson = new Gson();
                    String json = gson.toJson(resp);

                    Log.e(getClass().getName(), "BERHASIL SUBMIT COMPLAIN "+json);

                    Toast.makeText(getApplicationContext(), "Berhasil submit complain",Toast.LENGTH_SHORT).show();
                    message_body.setText("");
                    message_title.setText("");
                } else {
                    Toast.makeText(getApplicationContext(), resp.getErrorMsg(),Toast.LENGTH_SHORT).show();

                    //onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<responseSubmitComplain> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });

    }
    private void onFailRequest() {
        if (NetworkCheck.isConnect(this)) {
            showDialogFailed(R.string.msg_failed_load_data);
        } else {
            showDialogFailed(R.string.no_internet_text);
        }
    }

    public void showDialogFailed(@StringRes int msg) {
        if (dialog_failed != null && dialog_failed.isShowing()) return;

        dialog_failed = new DialogUtils(this).buildDialogWarning(-1, msg, R.string.TRY_AGAIN, R.drawable.img_no_connect, new CallbackDialog() {
            @Override
            public void onPositiveClick(Dialog dialog) {
                dialog.dismiss();
            }

            @Override
            public void onNegativeClick(Dialog dialog) {
            }
        });
        dialog_failed.show();
    }
    private boolean validateJudulPesan() {
        String str = message_title.getText().toString().trim();
        if (str.isEmpty()) {
            message_title_lyt.setError(getString(R.string.invalid_name));
            requestFocus(message_title);
            return false;
        } else {
            message_title_lyt.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateIsiPesan() {
        String str = message_body.getText().toString().trim();
        if (str.isEmpty()) {
            message_body_lyt.setError(getString(R.string.invalid_name));
            requestFocus(message_body);
            return false;
        } else {
            message_body_lyt.setErrorEnabled(false);
        }
        return true;
    }



    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
