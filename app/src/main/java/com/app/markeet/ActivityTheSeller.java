package com.app.markeet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.app.markeet.adapter.AdapterTheSeller;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Menu.MenuItemImg;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.model.Profile.view.Profile;
import com.app.markeet.model.Sellers.response.responseSearchSellerByCategory;
import com.app.markeet.model.Transaction.list.Transactions;
import com.app.markeet.model.Transaction.response.responseMyTransaction;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Hashtable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTheSeller extends AppCompatActivity {

    // activity transition
    public static void navigate(Activity activity, int category_id, String bahantitle) {
        Intent i = new Intent(activity, ActivityTheSeller.class);
        i.putExtra("category_id", category_id);
        i.putExtra("bahantitle", bahantitle);
        activity.startActivity(i);
    }

    private View parent_view;
    private RecyclerView recyclerView;
    private Call<get_product_categories> callbackCall;
    private AdapterTheSeller adapter;
    Hashtable<Object, Object> tabelvalue = new Hashtable<Object, Object>();
    ArrayList<MenuItemImg> dafmenu =  new ArrayList<MenuItemImg>();
    ThisApplication ta;
    int category_id=0;
    String bahantitle="";
    int TRANSACTION=23;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_seller);
        ta = ((ThisApplication)getApplicationContext());
        category_id = getIntent().getIntExtra("category_id", 0);
        bahantitle = getIntent().getStringExtra("bahantitle");
        parent_view = findViewById(android.R.id.content);
        initComponent();
        initToolbar();

        requestListOrder();

        recyclerView.setVisibility(View.VISIBLE);
    }


    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(bahantitle);
        Tools.systemBarLolipop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(ActivityTheSeller.this, 1));

        //set data and list adapter
        adapter = new AdapterTheSeller(ActivityTheSeller.this, new ArrayList<Profile>());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener(new AdapterTheSeller.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Profile obj) {
                ActivityProfile.navigate(ActivityTheSeller.this, obj.getId(),false);
            }
        });
    }

    private void requestListOrder() {

// Using the Retrofit
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","product");
        jsonObject.addProperty("action","search_seller_by_category");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("category_id",category_id);

        API api = RestAdapter2.createAPI();
        Call<responseSearchSellerByCategory> call = api.searchSeller(jsonObject);

        call.enqueue(new Callback<responseSearchSellerByCategory>() {
            @Override
            public void onResponse(Call<responseSearchSellerByCategory> call, Response<responseSearchSellerByCategory> response) {
                responseSearchSellerByCategory resp = response.body();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.getResponData());

                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<responseSearchSellerByCategory> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(ActivityTheSeller.this)) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TRANSACTION) {
            Bundle bundle = data.getExtras();

            //int adarubah = bundle.getInt("adarubah",0);
            //if(adarubah==1){
            requestListOrder();
            //}
        }
    }
}
