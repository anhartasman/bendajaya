package com.app.markeet;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class Download {

    //variabel
    String isiBitmap="";
    String isiJson="";
    String samsatusername="";
    String samsatpassword="";
    String tokenakses="";
    String nomorktpmemberbaru="";
    String namamemberbaru="";
    String tgllahirmemberbaru="";
    String nomorhandphonememberbaru="";
    String nomorkartumemberbaru="";
    String emailmemberbaru="";
    String merchantusername="";
    String merchantpassword="";
    String merchantgranttype="";
    String merchantclientid="";
    String merchantclientsecret="";

    String dida="";
    String tokennya="";
    int idklien=0;

    public  String kirimjson(String isijson,String urlnya){
        isiJson=isijson;

        String ergebnis;
        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                ergebnis = new SendingJSON().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,urlnya).get();
            } else {
                ergebnis = new SendingJSON().execute(urlnya).get();
            }

        } catch (InterruptedException e) {
            ergebnis = "Keine Daten InterruptedException";
        } catch (ExecutionException e) {
            ergebnis = "Keine Date ExecutionExceptionn";
        }
        return ergebnis;
    }
    public Bitmap downloadBild(String URL) {
        Bitmap bild;
        try {
            bild = new DownloadBildTask().execute(URL).get();
        } catch (InterruptedException e) {
            bild = null;
        } catch (ExecutionException e) {
            bild = null;
        }
        return bild;
    }

    public String downloadText(String URL) {
        String ergebnis;
        try {
            ergebnis = new DownloadTextTask().execute(URL).get();
        } catch (InterruptedException e) {
            ergebnis = "Keine Daten";
        } catch (ExecutionException e) {
            ergebnis = "Keine Daten";
        }
        return ergebnis;
    }

    private class DownloadTextTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                InputStream inputStream = downloadInhalt(urls[0]);
                String Text = InputStreamToString(inputStream);
                inputStream.close();
                return Text;

            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }

    private class DownloadBildTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                InputStream inputStream = downloadInhalt(urls[0]);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
                return bitmap;

            } catch (IOException e) {
                return null;
            }
        }
    }

    private InputStream downloadInhalt(String myurl) throws IOException {
        InputStream is;

        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            is = conn.getInputStream();
            return is;
        } catch (Exception e) {
            return null;
        }
    }

    private String InputStreamToString(InputStream stream) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }
        return total.toString();
    }

    public static String kirim(String targetURL, String urlParameters) throws IOException {
        String USER_AGENT = "Mozilla/5.0";
        String tujuan="http://pinpay.budayaindonesiaku.com/api/users/store";

        StringBuffer response;
        String hasil="";
        String POST_PARAMS = "name=bejo&email=bejo@yahoo.com&password=cuci123&identity_card_number=666555&id_card_number=555227&phone1=4442233&birth_date=1994-1-9";

        URL obj = new URL(tujuan);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Authorization", "Bearer NRsdzK6wfER40lAAo83YMC3afsdvkwutZMaO9sNZ");

        // For POST only - START
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
            //hasil=response.toString();
        } else {
            System.out.println("POST request not worked");
        }

        return hasil;
    }

    public  String kirimdatamember() throws IOException {
        String USER_AGENT = "Mozilla/5.0";
        String tujuan="http://pinpay.budayaindonesiaku.com/api/users/store";

        StringBuffer response;
        String hasil="";
        String POST_PARAMS = "name="+namamemberbaru+"&email="+emailmemberbaru+"&password=222&identity_card_number="+nomorktpmemberbaru+"&id_card_number="+nomorkartumemberbaru+"&phone1="+nomorhandphonememberbaru+"&birth_date="+tgllahirmemberbaru;

        URL obj = new URL(tujuan);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Authorization", "Bearer NRsdzK6wfER40lAAo83YMC3afsdvkwutZMaO9sNZ");

        // For POST only - START
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
            //hasil=response.toString();
        } else {
            System.out.println("POST request not worked");
        }

        return hasil;
    }




    private class SendingJSON extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                String USER_AGENT = "Mozilla/5.0";
                String tujuan=urls[0];

                StringBuffer response;
                String hasil="";

                URL obj = new URL(tujuan);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setRequestProperty("Content-Type: application/json", "Accept: application/json");
                //con.setRequestProperty("Authorization", "Bearer NRsdzK6wfER40lAAo83YMC3afsdvkwutZMaO9sNZ");

                // For POST only - START
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                //os.write(paymentMethod.toString().getBytes());
                os.write(isiJson.getBytes());
                os.flush();
                os.close();
                // For POST only - END

                int responseCode = con.getResponseCode();
                System.out.println("POST Response Code :: " + responseCode);

                if (responseCode == HttpURLConnection.HTTP_OK) { //success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                    // print result
                    System.out.println(response.toString());
                    hasil=response.toString();
                } else {
                    System.out.println("POST request not worked");
                }

                return hasil;
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }



    private class SendingGambar extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                String USER_AGENT = "Mozilla/5.0";
                String tujuan="http://semuahosting.net/kirimgambar.php";

                String boundary = "===" + System.currentTimeMillis() + "===";
                StringBuffer response;
                String hasil="";
                URL obj = new URL(tujuan);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", USER_AGENT);
                con.setDoOutput(true);
                OutputStream os = con.getOutputStream();
                //os.write(paymentMethod.toString().getBytes());
                os.write(isiBitmap.getBytes());
                os.flush();
                os.close();
                // For POST only - END

                int responseCode = con.getResponseCode();
                System.out.println("POST Response Code :: " + responseCode);

                if (responseCode == HttpURLConnection.HTTP_OK) { //success
                    BufferedReader in = new BufferedReader(new InputStreamReader(
                            con.getInputStream()));
                    String inputLine;
                    response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();

                    // print result
                    System.out.println(response.toString());
                    hasil=response.toString();
                } else {
                    System.out.println("POST request not worked");
                }

                return hasil;
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }
}