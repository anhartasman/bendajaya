package com.app.markeet;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.app.markeet.adapter.AdapterAddressInfo;
import com.app.markeet.adapter.AdapterSetCategories;
import com.app.markeet.model.Address.list.Address;
import com.app.markeet.model.ProductCategory.list.Category;
import com.app.markeet.utils.Tools;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anhar on 15/06/17.
 */
public class ActivitySetCategories extends AppCompatActivity {
    EditText edittext;
    TextView title;
    android.support.design.widget.AppBarLayout appbar_layout=null;
    android.support.v4.widget.NestedScrollView nested_content=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_go_back=null;
    TextView tv_go_back=null;
    JSONArray portlist = null;
    private RecyclerView recyclerView;
    private AdapterSetCategories mAdapter;



    int textlength = 0;


    ArrayList<Category> category_sort=new ArrayList<Category>();
    ThisApplication gv;


    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_categories);
        gv = ((ThisApplication)getApplicationContext());
        initToolbar();

        appbar_layout= (android.support.design.widget.AppBarLayout)findViewById(R.id.appbar_layout);
        nested_content= (android.support.v4.widget.NestedScrollView)findViewById(R.id.nested_content);
        lyt_go_back= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_go_back);
        tv_go_back= (TextView)findViewById(R.id.tv_go_back);

        lyt_go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                onBackPressed();


            }
        });

        //json_airport = appFunc.airportList();
/**
 SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.pref), MODE_PRIVATE);

 json_airport = sharedPreferences.getString("airport_domestik", "empty");
 **/

//        Log.d("airport",json_airport);

        // JSONObject obj_res = new JSONObject(json_airport);
        // JSONArray a = obj_res.getJSONArray("airport");


if(gv.list_categories!=null) {
    for (int i = 0; i < gv.list_categories.size(); i++) {
        Category b = gv.list_categories.get(i);
        category_sort.add(b);
    }
}

        edittext = (EditText) findViewById(R.id.EditText01);



        edittext.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                textlength = edittext.getText().length();
                category_sort.clear();

                Log.d("a","DIGANTI");

                for (int i = 0; i < gv.list_categories.size(); i++) {
                    Category b=gv.list_categories.get(i);
                    String isi = b.getCategory();

                    if (textlength <= isi.length()) {

                        if (isi.toLowerCase().contains(edittext.getText().toString().toLowerCase())) {
                            category_sort.add(b);
                        }
                    }

                }

                Log.d("a","category_sort "+category_sort.size());
                displayApiResult(category_sort);
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //set data and list adapter
        mAdapter = new AdapterSetCategories(this, recyclerView, new ArrayList<Category>());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new AdapterSetCategories.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Category obj, int position) {
                //ActivityAddressDetails.navigate(ActivityAddress.this, Long.valueOf(obj.getId()), false);
                Log.d(getClass().getName(),"PILIHAN "+String.valueOf(position));
                setpilih(position);

            }
        });

        displayApiResult(gv.list_categories);
        //akhir onCreate

    }



    private void displayApiResult(final List<Category> items) {
        mAdapter.resetListData();
        mAdapter.insertData(items);
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_set_categories);
        Tools.systemBarLolipop(this);
    }


    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putString("bdrp", "");
        b.putString("city", "");
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void setpilih(int i){
        category_sort.get(i).setSelected(!category_sort.get(i).getSelected());
        displayApiResult(category_sort);

        for(int a=0; a<gv.list_categories.size(); a++){
            if(gv.list_categories.get(a).getCategory().equalsIgnoreCase(category_sort.get(i).getCategory())){
                gv.list_categories.get(a).setSelected(category_sort.get(i).getSelected());
                break;
            }
        }
    }

}
