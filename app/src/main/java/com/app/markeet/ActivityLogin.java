package com.app.markeet;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Akun.response.responseLogin;
import com.app.markeet.model.BuyerProfile;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.utils.CallbackDialog;
import com.app.markeet.utils.DialogUtils;
import com.app.markeet.utils.NetworkCheck;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity {
    android.support.design.widget.TextInputLayout buyer_name_lyt=null;
    EditText username=null;
    android.support.design.widget.TextInputLayout password_lyt=null;
    EditText password=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_submit=null;
    TextView tv_add_cart=null;
    com.balysv.materialripple.MaterialRippleLayout lyt_register=null;
    TextView tv_register_cart=null;
    private View parent_view;
    private Dialog dialog_failed = null;
    ThisApplication ta;
    ProgressDialog progressDialog = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        parent_view = findViewById(android.R.id.content);
        ta = ((ThisApplication)getApplicationContext());


        buyer_name_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.buyer_name_lyt);
        username= (EditText)findViewById(R.id.username);
        password_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.password_lyt);
        password= (EditText)findViewById(R.id.password);
        lyt_submit= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_submit);
        tv_add_cart= (TextView)findViewById(R.id.tv_add_cart);
        lyt_register= (com.balysv.materialripple.MaterialRippleLayout)findViewById(R.id.lyt_register);
        tv_register_cart= (TextView)findViewById(R.id.tv_register_cart);

        progressDialog = new ProgressDialog(ActivityLogin.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_data));

        lyt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekValidLogin();
            }
        });

        lyt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ActivityLogin.this, ActivityRegister.class);
                startActivity(intent);
            }
        });
        //akhir onCreate
    }

    private void cekValidLogin() {
        int bisalogin=1;
        if (!validateUserName()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }
        if (!validatePassword()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            bisalogin=0;
            return;
        }

        if(bisalogin==1){
            delayPostLogin();
        }


        // hide keyboard
        hideKeyboard();

    }

    private void delayPostLogin() {
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                postLogin();
            }
        }, 2000);
    }
    public void postLogin(){

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","login");
        jsonObject.addProperty("username",username.getText().toString().trim());
        jsonObject.addProperty("password",password.getText().toString().trim());


        API api = RestAdapter2.createAPI();
        Call<responseLogin> call = api.postLogin(jsonObject);

        call.enqueue(new Callback<responseLogin>() {
            @Override
            public void onResponse(Call<responseLogin> call, Response<responseLogin> response) {
                responseLogin resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Gson gson = new Gson();
                    String json = gson.toJson(resp.getAkunConfig());

                    Log.e(getClass().getName(), "BERHASIL LOGIN "+json);


                    Intent intent = new Intent(ActivityLogin.this, ActivityMain.class);


                    String MyPREFERENCES = "MyPref";
                    SharedPreferences sharedPrefs;
                    SharedPreferences.Editor editor;
                    sharedPrefs = getSharedPreferences(MyPREFERENCES,
                            Context.MODE_PRIVATE);
                    editor = sharedPrefs.edit();
                    editor.putInt("sudahlogin", 1);
                    editor.putString("isiprofil", json);
                    editor.commit();

                    ta.akunConfig=resp.getAkunConfig();
                    Log.d(getClass().getName(),"ISI NAME : "+ta.akunConfig.getName());


                    startActivity(intent);

                } else {
                    //onFailRequest(resp.getErrorMsg());
                    Toast.makeText(getApplicationContext(), resp.getErrorMsg(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<responseLogin> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });
    }
    private void onFailRequest(String msg) {
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true,msg);
        } else {
            showFailedView(true,getString(R.string.no_internet_text));
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private boolean validateUserName() {
        String str = username.getText().toString().trim();
        if (str.isEmpty()) {
            buyer_name_lyt.setError(getString(R.string.invalid_name));
            requestFocus(username);
            return false;
        } else {
            buyer_name_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        String str = password.getText().toString().trim();
        if (str.isEmpty()) {
            password_lyt.setError(getString(R.string.invalid_name));
            requestFocus(password);
            return false;
        } else {
            password_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {

    }

}
