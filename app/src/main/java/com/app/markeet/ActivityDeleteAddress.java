package com.app.markeet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.connection.callbacks.CallbackNewsInfoDetails;
import com.app.markeet.model.Address.response.responseAddAddress;
import com.app.markeet.model.NewsInfo;
import com.app.markeet.utils.NetworkCheck;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityDeleteAddress extends AppCompatActivity {

    //deklarasi variabel
    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";
    android.support.design.widget.TextInputLayout address_title_lyt=null;
    EditText address_title=null;
    android.support.design.widget.TextInputLayout address_lyt=null;
    EditText address=null;
    android.support.design.widget.AppBarLayout appbar_layout=null;
    android.support.v4.widget.NestedScrollView nested_content=null;
    MaterialRippleLayout lyt_delete_address=null;
    TextView tv_delete_address=null;
    ProgressDialog progressDialog = null;
    ThisApplication ta;

    // activity transition
    public static void navigate(Activity activity,Boolean from_notif) {
        Intent i = navigateBase(activity,from_notif);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context,Boolean from_notif) {
        Intent i = new Intent(context, ActivityAddProduct.class);
        i.putExtra(EXTRA_FROM_NOTIF,from_notif);
        return i;
    }

    private Long news_id;
    private Boolean from_notif;

    // extra obj
    private NewsInfo newsInfo;

    private Call<CallbackNewsInfoDetails> callbackCall = null;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private WebView webview;
    int adarubah=0;
    int form_address_id=0;
    String form_address_title="";
    String form_address="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_address);
        ta = ((ThisApplication)getApplicationContext());
        form_address_id = getIntent().getIntExtra("address_id", 0);
        form_address_title = getIntent().getStringExtra("address_title");
        form_address = getIntent().getStringExtra("address");

        news_id = getIntent().getLongExtra(EXTRA_OBJECT_ID, -1L);
        from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);

        initComponent();
        initToolbar();
        requestAction();

        //akhir onCreate
    }

    private void initComponent() {
        parent_view = findViewById(android.R.id.content);

        address_title_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.address_title_lyt);
        address_title= (EditText)findViewById(R.id.address_title);
        address_lyt= (android.support.design.widget.TextInputLayout)findViewById(R.id.address_lyt);
        address= (EditText)findViewById(R.id.address);
        appbar_layout= (android.support.design.widget.AppBarLayout)findViewById(R.id.appbar_layout);
        nested_content= (android.support.v4.widget.NestedScrollView)findViewById(R.id.nested_content);
        lyt_delete_address= (MaterialRippleLayout)findViewById(R.id.lyt_delete_address);
        tv_delete_address= (TextView)findViewById(R.id.tv_delete_address);

        progressDialog = new ProgressDialog(ActivityDeleteAddress.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_address));

        lyt_delete_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        address.setText(form_address);
        address_title.setText(form_address_title);

        //akhir initComponent
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_checkout);
    }

    private void requestAction() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestNewsInfoDetailsApi();
            }
        }, 1000);
    }

    private void onFailRequest(String msg) {
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, msg);
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void submitForm() {
        int lanjutsubmit=1;
        if (!validateAddress()) {
            Snackbar.make(parent_view, R.string.invalid_address, Snackbar.LENGTH_SHORT).show();
            lanjutsubmit=0;
            return;
        }
        if (!validateAddressTitle()) {
            Snackbar.make(parent_view, R.string.invalid_address_title, Snackbar.LENGTH_SHORT).show();
            lanjutsubmit=0;
            return;
        }

        if(lanjutsubmit==1) {
            // hide keyboard
            hideKeyboard();

            // show dialog confirmation
            dialogConfirmSubmitForm();
        }
    }

    private void requestNewsInfoDetailsApi() {
        /**
         API api = RestAdapter.createAPI();
         callbackCall = api.getNewsDetails(news_id);
         callbackCall.enqueue(new Callback<CallbackNewsInfoDetails>() {
        @Override
        public void onResponse(Call<CallbackNewsInfoDetails> call, Response<CallbackNewsInfoDetails> response) {
        CallbackNewsInfoDetails resp = response.body();
        if (resp != null && resp.status.equals("success")) {
        newsInfo = resp.news_info;
        displayPostData();
        } else {
        onFailRequest();
        }
        }

        @Override
        public void onFailure(Call<CallbackNewsInfoDetails> call, Throwable t) {
        Log.e("onFailure", t.getMessage());
        if (!call.isCanceled()) onFailRequest();
        }

        });
         **/
    }


    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    private boolean validateAddressTitle() {
        String str = address_title.getText().toString().trim();
        if (str.isEmpty()) {
            address_title_lyt.setError(getString(R.string.invalid_address_title));
            requestFocus(address_title);
            return false;
        } else {
            address_title_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateAddress() {
        String str = address.getText().toString().trim();
        if (str.isEmpty()) {
            address_lyt.setError(getString(R.string.invalid_address));
            requestFocus(address);
            return false;
        } else {
            address_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webview != null) webview.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webview != null) webview.onPause();
    }

    public void dialogConfirmSubmitForm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirmation);
        builder.setMessage(getString(R.string.confirm_delete_address));
        builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitOrderData();
            }
        });
        builder.setNegativeButton(R.string.NO, null);
        builder.show();
    }

    private void delaySubmitOrderData() {
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitFormData();
            }
        }, 2000);
    }

    private void submitFormData() {
        // prepare insert form data
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","deleteAddress");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("address_id",form_address_id);
        jsonObject.addProperty("address_title",address_title.getText().toString());
        jsonObject.addProperty("address",address.getText().toString());

        API api = RestAdapter2.createAPI();
        Call<responseAddAddress> call = api.addAddress(jsonObject);

        call.enqueue(new Callback<responseAddAddress>() {
            @Override
            public void onResponse(Call<responseAddAddress> call, Response<responseAddAddress> response) {
                responseAddAddress resp = response.body();

                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Toast.makeText(ActivityDeleteAddress.this, "Success",
                            Toast.LENGTH_SHORT).show();
                    adarubah=1;
                    onBackPressed();

                } else {
                    Toast.makeText(ActivityDeleteAddress.this, "Failed",
                            Toast.LENGTH_SHORT).show();
                    onFailRequest(resp.getErrorMsg()==null? "error":resp.getErrorMsg());
                }

            }

            @Override
            public void onFailure(Call<responseAddAddress> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });


        //akhir submitFormData
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle b = new Bundle();
        b.putInt("adarubah", adarubah);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            onBackAction();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Bundle b = new Bundle();
        b.putInt("adarubah", adarubah);
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(RESULT_OK, intent);
        finish();
        onBackAction();
    }

    private void onBackAction() {
        if (from_notif) {
            if (ActivityMain.active) {
                finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), ActivitySplash.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }

}
