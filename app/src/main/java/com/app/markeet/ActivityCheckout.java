package com.app.markeet;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.adapter.AdapterShoppingCart;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.connection.callbacks.CallbackOrder;
import com.app.markeet.connection.callbacks.CallbackSubmitOrder;
import com.app.markeet.data.DatabaseHandler;
import com.app.markeet.data.SharedPref;
import com.app.markeet.model.BuyerProfile;
import com.app.markeet.model.Cart;
import com.app.markeet.model.CheckOngkir;
import com.app.markeet.model.Checkout;
import com.app.markeet.model.Info;
import com.app.markeet.model.Ongkir.detail.Ongkir;
import com.app.markeet.model.Ongkir.response.responseCheckOngkir;
import com.app.markeet.model.Order;
import com.app.markeet.model.ProductOrder;
import com.app.markeet.model.ProductOrderDetail;
import com.app.markeet.utils.CallbackDialog;
import com.app.markeet.utils.DialogUtils;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCheckout extends AppCompatActivity {

    private View parent_view;
    private Spinner shipping;
    private ImageButton bt_date_shipping;
    private TextView date_shipping;
    private RecyclerView recyclerView;
    private MaterialRippleLayout lyt_add_cart;
    private TextView total_order, tax, price_tax, total_fees, address,price_ongkir;
    private TextInputLayout buyer_name_lyt, phone_lyt, comment_lyt;
    private EditText buyer_name, phone, comment;
    private Button btnChooseAddress;

    private DatePickerDialog datePickerDialog;
    private AdapterShoppingCart adapter;
    private DatabaseHandler db;
    private SharedPref sharedPref;
    private BuyerProfile buyerProfile;
    private Long date_ship_millis = 0L;
    private Double _total_fees = 0D;
    private String _total_fees_str;

    private Call<CallbackOrder> callbackCall = null;
    // construct dialog progress
    ProgressDialog progressDialog = null;
    Integer delivery_address_id=0;
    String delivery_address_title="";
    String delivery_address="";
    Integer delivery_transport_service_id=0;
    ThisApplication ta;
    Ongkir dataOngkir=null;
    int pajak=0;
    int REQUEST_ADDRESS=666;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ta = ((ThisApplication)getApplicationContext());
        pajak=Integer.valueOf(ta.system_config.getTax());
        db = new DatabaseHandler(this);
        sharedPref = new SharedPref(this);
        buyerProfile = sharedPref.getBuyerProfile();

        initToolbar();
        iniComponent();
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_checkout);
        Tools.systemBarLolipop(this);
    }

    private void iniComponent() {
        parent_view = findViewById(android.R.id.content);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        lyt_add_cart = (MaterialRippleLayout) findViewById(R.id.lyt_add_cart);

        btnChooseAddress = (Button) findViewById(R.id.btnChooseAddress);

        // cost view
        total_order = (TextView) findViewById(R.id.total_order);
        tax = (TextView) findViewById(R.id.tax);
        price_tax = (TextView) findViewById(R.id.price_tax);
        total_fees = (TextView) findViewById(R.id.total_fees);

        // form view
        buyer_name = (EditText) findViewById(R.id.buyer_name);
        phone = (EditText) findViewById(R.id.phone);
        address = (TextView) findViewById(R.id.address);
        price_ongkir = (TextView) findViewById(R.id.price_ongkir);
        comment = (EditText) findViewById(R.id.comment);

        buyer_name.addTextChangedListener(new CheckoutTextWatcher(buyer_name));
        phone.addTextChangedListener(new CheckoutTextWatcher(phone));
        comment.addTextChangedListener(new CheckoutTextWatcher(comment));

        buyer_name_lyt = (TextInputLayout) findViewById(R.id.buyer_name_lyt);
        phone_lyt = (TextInputLayout) findViewById(R.id.phone_lyt);
        comment_lyt = (TextInputLayout) findViewById(R.id.comment_lyt);
        shipping = (Spinner) findViewById(R.id.shipping);
        bt_date_shipping = (ImageButton) findViewById(R.id.bt_date_shipping);
        date_shipping = (TextView) findViewById(R.id.date_shipping);
        List<String> shipping_list = new ArrayList<>();
        shipping_list.add(getString(R.string.choose_shipping));
        for(int i=0; i<ta.transportproviders.size(); i++){
            shipping_list.add(ta.transportproviders.get(i).getName());
        }

        // Initialize and set Adapter
        ArrayAdapter adapter_shipping = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, shipping_list.toArray());
        adapter_shipping.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shipping.setAdapter(adapter_shipping);

        shipping.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
         if(position>0) {
            delivery_transport_service_id = ta.transportproviders.get((position - 1)).getId();

                 if (delivery_address_id != 0) {
                     delayCheckOngkir();
                 }
        }else{
                 delivery_transport_service_id=0;
        }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        progressDialog = new ProgressDialog(ActivityCheckout.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_submit_checkout));

        bt_date_shipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogDatePicker();
            }
        });

        lyt_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        btnChooseAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityCheckout.this, ActivityAddress.class);
                ActivityCheckout.this.startActivityForResult(i, REQUEST_ADDRESS);
            }
        });

        //akhir iniComponent
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ADDRESS) {
            if(data!=null) {
                Bundle bundle = data.getExtras();

                delivery_address_id = bundle.getInt("delivery_address_id", 0);
                delivery_address_title = bundle.getString("delivery_address_title");
                delivery_address = bundle.getString("delivery_address");
                address.setText("Alamat\n" + delivery_address);

                if (delivery_transport_service_id != 0) {
                    delayCheckOngkir();
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayData();
    }

    private void displayData() {
        List<Cart> items = db.getActiveCartList();
        adapter = new AdapterShoppingCart(this, false, items,pajak);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        setTotalPrice();
        if (buyerProfile != null) {
            buyer_name.setText(buyerProfile.name);
            phone.setText(buyerProfile.phone);
            address.setText(buyerProfile.address);
        }
    }

    private void setTotalPrice() {
        List<Cart> items = adapter.getItem();
        Double _total_order = 0D, _price_tax, _price_ongkir = 0D;
        String _total_order_str, _price_tax_str,_price_ongkir_str;
        for (Cart c : items) {
            Integer harga=Integer.valueOf(String.valueOf(c.amount * c.price_item.intValue()));
            Integer hargabaru= Tools.tambahpajak(harga,pajak);
            Double dblhrgbaru=Double.valueOf(hargabaru);

            _total_order = _total_order + (dblhrgbaru);
        }
        _price_tax = _total_order * Integer.valueOf(ta.system_config.getTax()) / 100;
        if(dataOngkir!=null) {
            _price_ongkir = Double.valueOf(dataOngkir.getOngkir());
            _total_fees = _total_order  + _price_ongkir;
        }else{
            _total_fees=0D;
        }
        _price_ongkir_str = Tools.getFormattedPrice(_price_ongkir, this);
        price_ongkir.setText(_price_ongkir_str.toString());
        _price_tax_str = Tools.getFormattedPrice(_price_tax, this);
        _total_order_str = Tools.getFormattedPrice(_total_order, this);
        _total_fees_str = Tools.getFormattedPrice(_total_fees, this);

        // set to display
        total_order.setText(_total_order_str);
        tax.setText(getString(R.string.tax) + ta.system_config.getTax() + "%");
        price_tax.setText(_price_tax_str);
        total_fees.setText(_total_fees_str);
    }


    private void submitForm() {
        if (!validateName()) {
            Snackbar.make(parent_view, R.string.invalid_name, Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (!validatePhone()) {
            Snackbar.make(parent_view, R.string.invalid_phone, Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (!validateShipping()) {
            Snackbar.make(parent_view, R.string.invalid_shipping, Snackbar.LENGTH_SHORT).show();
            return;
        }
        if (!validateDateShip()) {
            Snackbar.make(parent_view, R.string.invalid_date_ship, Snackbar.LENGTH_SHORT).show();
            return;
        }
        if(delivery_address_id==0){
            Snackbar.make(parent_view, R.string.select_delivery_address, Snackbar.LENGTH_SHORT).show();
            return;
        }

        buyerProfile = new BuyerProfile();
        buyerProfile.name = buyer_name.getText().toString();
        buyerProfile.phone = phone.getText().toString();
        buyerProfile.address = delivery_address.toString();
        buyerProfile.address_id = delivery_address_id.toString();
        sharedPref.setBuyerProfile(buyerProfile);

        // hide keyboard
        hideKeyboard();

        // show dialog confirmation
        dialogConfirmCheckout();
    }

    private void submitCheckOngkir() {
        ArrayList<Integer> aridp=new ArrayList<Integer>();
        for (Cart c : adapter.getItem()) {
            ProductOrderDetail pod = new ProductOrderDetail(c.product_id, c.product_name, c.amount, c.price_item);
            aridp.add(Integer.valueOf(pod.product_id.toString()));
        }

        CheckOngkir cekir = new CheckOngkir();
        cekir.sessid = ta.akunConfig.getSessid();
        cekir.transport_service_id=delivery_transport_service_id;
        cekir.destination=delivery_address_id;
        cekir.aridp=aridp;

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","transaction");
        jsonObject.addProperty("action","check_ongkir");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());
        jsonObject.addProperty("transport_service_id",delivery_transport_service_id);
        jsonObject.addProperty("destination",delivery_address_id);


        API api = RestAdapter2.createAPI();
        Call<responseCheckOngkir> call = api.submitCheckOngkir(cekir);

        call.enqueue(new Callback<responseCheckOngkir>() {
            @Override
            public void onResponse(Call<responseCheckOngkir> call, Response<responseCheckOngkir> response) {
                responseCheckOngkir resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                   // post_total = addrs.size();
                    //displayApiResult(addrs);
                    dataOngkir=resp.getDataOngkir();
                    setTotalPrice();
                } else {
                    onFailRequest(resp.getErrorMsg());
                }

            }

            @Override
            public void onFailure(Call<responseCheckOngkir> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(t.getMessage());
            }

        });

    }

    private void onFailRequest(String msg) {
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, msg);
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delayCheckOngkir();
            }
        });
    }
    private void submitOrderData() {
        // prepare checkout data
        Checkout checkout = new Checkout();
        ProductOrder productOrder = new ProductOrder(buyerProfile, shipping.getSelectedItem().toString(), date_ship_millis, comment.getText().toString().trim());
        productOrder.status = "WAITING";
        productOrder.total_fees = _total_fees;
        productOrder.tax = Double.valueOf(ta.system_config.getTax());
        productOrder.transport_service_id = delivery_transport_service_id;
        // to support notification
        productOrder.serial = Tools.getDeviceID(this);
        checkout.sessid=ta.akunConfig.getSessid();
        checkout.product_order = productOrder;
        checkout.product_order_detail = new ArrayList<>();
        for (Cart c : adapter.getItem()) {
            ProductOrderDetail pod = new ProductOrderDetail(c.product_id, c.product_name, c.amount, c.price_item);
            checkout.product_order_detail.add(pod);
        }

        // submit data to server
        API api = RestAdapter2.createAPI();
        callbackCall = api.submitProductOrderToAPI(checkout);
        callbackCall.enqueue(new Callback<CallbackOrder>() {
            @Override
            public void onResponse(Call<CallbackOrder> call, Response<CallbackOrder> response) {
                CallbackOrder resp = response.body();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    Order order = new Order(Long.valueOf(resp.getResponData().getIdTrx()), resp.getResponData().getNoTrx(), _total_fees_str);
                    for (Cart c : adapter.getItem()) {
                        c.order_id = order.id;
                        order.cart_list.add(c);
                    }
                    db.saveOrder(order);
                    dialogSuccess(order.code);
                } else {
                    //dialogFailedRetry();
                    Toast.makeText(getApplicationContext(), resp.getErrorMsg(),Toast.LENGTH_SHORT).show();

                    onFailRequest(resp.getErrorMsg());
                }
/**
                if (resp != null && resp.status.equals("success")) {
                    Order order = new Order(resp.data.id, resp.data.code, _total_fees_str);
                    for (Cart c : adapter.getItem()) {
                        c.order_id = order.id;
                        order.cart_list.add(c);
                    }
                    db.saveOrder(order);
                    dialogSuccess(order.code);
                } else {
                    dialogFailedRetry();
                }
**/
            }

            @Override
            public void onFailure(Call<CallbackOrder> call, Throwable t) {
//                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) dialogFailedRetry();
            }
        });
    }

    private void delayCheckOngkir() {
        progressDialog.setMessage(getString(R.string.content_submit_checkongkir));
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitCheckOngkir();
            }
        }, 1000);
    }

    // give delay when submit data to give good UX
    private void delaySubmitOrderData() {
        progressDialog.setMessage(getString(R.string.content_submit_checkout));
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                submitOrderData();
            }
        }, 2000);
    }

    public void dialogConfirmCheckout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.confirmation);
        builder.setMessage(getString(R.string.confirm_checkout));
        builder.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitOrderData();
            }
        });
        builder.setNegativeButton(R.string.NO, null);
        builder.show();
    }

    public void dialogFailedRetry() {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.failed);
        builder.setMessage(getString(R.string.failed_checkout));
        builder.setPositiveButton(R.string.TRY_AGAIN, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                delaySubmitOrderData();
            }
        });
        builder.setNegativeButton(R.string.SETTING, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(getApplicationContext(), ActivitySettings.class));
            }
        });
        builder.show();
    }

    public void dialogSuccess(String code) {
        progressDialog.dismiss();
        Dialog dialog = new DialogUtils(this).buildDialogInfo(
                getString(R.string.success_checkout),
                String.format(getString(R.string.msg_success_checkout), code),
                getString(R.string.OK),
                R.drawable.img_checkout_success,
                new CallbackDialog() {
                    @Override
                    public void onPositiveClick(Dialog dialog) {
                        finish();
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeClick(Dialog dialog) {
                    }
                });
        dialog.show();
    }

    private void dialogDatePicker() {
        Calendar cur_calender = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, R.style.DatePickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int _year, int _month, int _day) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, _year);
                calendar.set(Calendar.MONTH, _month);
                calendar.set(Calendar.DAY_OF_MONTH, _day);
                date_ship_millis = calendar.getTimeInMillis();
                date_shipping.setText(Tools.getFormattedDateSimple(date_ship_millis));
                datePickerDialog.dismiss();
            }
        }, cur_calender.get(Calendar.YEAR), cur_calender.get(Calendar.MONTH), cur_calender.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.setCancelable(true);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    // validation method
    private boolean validateName() {
        String str = buyer_name.getText().toString().trim();
        if (str.isEmpty()) {
            buyer_name_lyt.setError(getString(R.string.invalid_name));
            requestFocus(buyer_name);
            return false;
        } else {
            buyer_name_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePhone() {
        String str = phone.getText().toString().trim();
        if (str.isEmpty()) {
            phone_lyt.setError(getString(R.string.invalid_phone));
            requestFocus(phone);
            return false;
        } else {
            phone_lyt.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateShipping() {
        int pos = shipping.getSelectedItemPosition();
        if (pos == 0) {
            return false;
        }
        return true;
    }

    private boolean validateDateShip() {
        if (date_ship_millis == 0L) {
            return false;
        }
        return true;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private class CheckoutTextWatcher implements TextWatcher {
        private View view;

        private CheckoutTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.name:
                    validateName();
                    break;
                case R.id.phone:
                    validatePhone();
                    break;
            }
        }
    }
}
