package com.app.markeet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.markeet.adapter.AdapterMyProduct;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.connection.callbacks.CallbackProduct;
import com.app.markeet.data.AppConfig;
import com.app.markeet.model.ProductCategory.list.Category;
import com.app.markeet.model.Products.list.Product;
import com.app.markeet.model.Products.response.get_products;
import com.app.markeet.model.Products.response.responseMyProducts;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyProducts extends AppCompatActivity {
    private static final String EXTRA_OBJECT = "key.EXTRA_OBJECT";

    // activity transition
    public static void navigate(Activity activity, Category obj) {
        Intent i = new Intent(activity, ActivityMyProducts.class);
        i.putExtra(EXTRA_OBJECT, obj);
        activity.startActivity(i);
    }

    // extra obj
    private Category category;

    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackProduct> callbackCall = null;
    ThisApplication ta;
    private RecyclerView recyclerView;
    private AdapterMyProduct mAdapter;

    private int post_total = 0;
    private int failed_page = 0;
    Hashtable<Object, Object> tabelvalue = new Hashtable<Object, Object>();
    FloatingActionButton fabAddProduct=null;
    int CRUD_PRODUCT=121;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_products);
        parent_view = findViewById(android.R.id.content);
        category = (Category) getIntent().getSerializableExtra(EXTRA_OBJECT);
        ta = ((ThisApplication)getApplicationContext());

        initComponent();
        initToolbar();

        displayCategoryData(category);

        requestAction(1);
    }

    private void initComponent() {
        fabAddProduct = (FloatingActionButton) findViewById(R.id.fabAddProduct);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, Tools.getGridSpanCount(this)));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterMyProduct(this, recyclerView, new ArrayList<Product>());
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterMyProduct.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Product obj, int position) {

                Intent intent = new Intent(ActivityMyProducts.this, ActivityMyProductDetail.class);
                intent.putExtra("key.EXTRA_OBJECT_ID",Long.valueOf(obj.getId()));
                ActivityMyProducts.this.startActivityForResult(intent, CRUD_PRODUCT);

            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterMyProduct.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });
        fabAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(ActivityMyProducts.this, ActivityAddProduct.class);
                ActivityMyProducts.this.startActivityForResult(i, CRUD_PRODUCT);
            }
        });

    }

    private void displayCategoryData(Category c) {
        ((AppBarLayout) findViewById(R.id.app_bar_layout)).setBackgroundColor(Color.parseColor(c.getColor()));
        ((TextView) findViewById(R.id.name)).setText(c.getCategory());
        ((TextView) findViewById(R.id.brief)).setText(c.getInfo());
        ImageView icon = (ImageView) findViewById(R.id.icon);
        Tools.displayImageOriginal(this, icon, c.getIcon());
        Tools.setSystemBarColorDarker(this, c.getColor());
        if (AppConfig.TINT_CATEGORY_ICON) {
            icon.setColorFilter(Color.WHITE);
        }

        // analytics track
        ThisApplication.getInstance().saveLogEvent(c.getId(), c.getCategory(), "CATEGORY_DETAILS");
    }


    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_category_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if(item_id == android.R.id.home){
            super.onBackPressed();
        } else if(item_id == R.id.action_search){
            ActivitySearch.navigate(ActivityMyProducts.this, category);
        } else if(item_id == R.id.action_cart){
            Intent i = new Intent(this, ActivityShoppingCart.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void displayApiResult(final List<Product> items) {
        mAdapter.resetListData();
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) showNoItemView(true);
    }

    private void requestListProduct(final int page_no) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","product");
        jsonObject.addProperty("action","my_products");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());


        API api = RestAdapter2.createAPI();
        Call<responseMyProducts> call = api.myProducts(jsonObject);

        call.enqueue(new Callback<responseMyProducts>() {
            @Override
            public void onResponse(Call<responseMyProducts> call, Response<responseMyProducts> response) {
                responseMyProducts resp = response.body();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    post_total = resp.getProducts().size();
                    displayApiResult(resp.getProducts());
                } else {
                    onFailRequest(page_no);
                }

            }

            @Override
            public void onFailure(Call<responseMyProducts> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });

    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListProduct(page_no);
            }
        }, 1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) findViewById(R.id.lyt_no_item);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CRUD_PRODUCT) {
            if(data!=null) {
                Bundle bundle = data.getExtras();

                int adarubah = bundle.getInt("adarubah", 0);
                if (adarubah == 1) {
                    requestAction(1);
                }
            }
        }
    }
}
