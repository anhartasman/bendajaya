package com.app.markeet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.adapter.AdapterAddressInfo;
import com.app.markeet.adapter.AdapterNewsInfo;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.connection.callbacks.CallbackNewsInfo;
import com.app.markeet.data.Constant;
import com.app.markeet.model.Address.list.Address;
import com.app.markeet.model.Address.response.responseGetAddress;
import com.app.markeet.model.Akun.response.responseGetAccount;
import com.app.markeet.model.NewsInfo;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAddress extends AppCompatActivity {

    //deklarasi variabel
    private View parent_view;
    private RecyclerView recyclerView;
    private AdapterAddressInfo mAdapter;
    private Call<CallbackNewsInfo> callbackCall = null;

    private int post_total = 0;
    private int failed_page = 0;
    ThisApplication ta;
    private MaterialRippleLayout lyt_add_address;
    ProgressDialog progressDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        parent_view = findViewById(android.R.id.content);
        ta = ((ThisApplication)getApplicationContext());
        initToolbar();
        iniComponent();
    }

    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_address);
        Tools.systemBarLolipop(this);
    }

    public void iniComponent() {

        progressDialog = new ProgressDialog(ActivityAddress.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(R.string.title_please_wait);
        progressDialog.setMessage(getString(R.string.content_getting_data));

        lyt_add_address = (MaterialRippleLayout) findViewById(R.id.lyt_add_address);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //set data and list adapter
        mAdapter = new AdapterAddressInfo(this, recyclerView, new ArrayList<Address>());
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterAddressInfo.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Address obj, int position,int pilihan) {
                //ActivityAddressDetails.navigate(ActivityAddress.this, Long.valueOf(obj.getId()), false);
                Log.d(getClass().getName(),"PILIHAN "+String.valueOf(pilihan));
                if(pilihan==1) {

                    Bundle b = new Bundle();
                    b.putInt("delivery_address_id", obj.getId());
                    b.putString("delivery_address_title", obj.getLabel());
                    b.putString("delivery_address", obj.getAddress());
                    Intent intent = new Intent();
                    intent.putExtras(b);
                    setResult(RESULT_OK, intent);
                    finish();
                    onBackPressed();

                }else if(pilihan==2){
                    Intent intent = new Intent(ActivityAddress.this, ActivityEditAddress.class);
                    intent.putExtra("address_id",obj.getId());
                    intent.putExtra("address_title",obj.getLabel());
                    intent.putExtra("address",obj.getAddress());
                    ActivityAddress.this.startActivityForResult(intent, 111);
                }else if(pilihan==3){
                    Intent intent = new Intent(ActivityAddress.this, ActivityDeleteAddress.class);
                    intent.putExtra("address_id",obj.getId());
                    intent.putExtra("address_title",obj.getLabel());
                    intent.putExtra("address",obj.getAddress());
                    ActivityAddress.this.startActivityForResult(intent, 111);
                }

            }
        });



        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterAddressInfo.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        requestAction(1);


        lyt_add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //ActivityAddAddress.navigate(ActivityAddress.this);
                Intent intent = new Intent(ActivityAddress.this, ActivityAddAddress.class);
                ActivityAddress.this.startActivityForResult(intent, 111);
            }
        });



        //akhir initComponent
    }

    private void displayApiResult(final List<Address> items) {
        mAdapter.resetListData();
        mAdapter.insertData(items);
        if (items.size() == 0) {
            showNoItemView(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public Context getAct(){
        return this;
    }


    private void requestListAddress() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","getAddress");
        jsonObject.addProperty("sessid",ta.akunConfig.getSessid());

        API api = RestAdapter2.createAPI();
        Call<responseGetAddress> call = api.getAddress(jsonObject);

        call.enqueue(new Callback<responseGetAddress>() {
            @Override
            public void onResponse(Call<responseGetAddress> call, Response<responseGetAddress> response) {
                responseGetAddress resp = response.body();
                List<Address> addrs = response.body().getAddress();
                progressDialog.dismiss();
                if (resp != null && resp.getErrorCode().equals("000")) {
                    post_total = addrs.size();
                    displayApiResult(addrs);

                } else {
                    onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<responseGetAddress> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
/**
        API api = RestAdapter.createAPI();
        callbackCall = api.getListNewsInfo(page_no, Constant.NEWS_PER_REQUEST, null);
        callbackCall.enqueue(new Callback<CallbackNewsInfo>() {
            @Override
            public void onResponse(Call<CallbackNewsInfo> call, Response<CallbackNewsInfo> response) {
                CallbackNewsInfo resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    post_total = resp.count_total;
                    displayApiResult(resp.news_infos);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<CallbackNewsInfo> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });
 **/
    }

    private void onFailRequest() {
        mAdapter.setLoaded();
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "");
        showNoItemView(false);
        if (page_no == 1) {
        } else {
            mAdapter.setLoading();
        }
        progressDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListAddress();
            }
        }, 1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 111) {
            Bundle bundle = data.getExtras();

                int adarubah = bundle.getInt("adarubah",0);
                if(adarubah==1){
                    requestAction(1);
                }
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) findViewById(R.id.lyt_no_item);
        ((TextView) findViewById(R.id.no_item_message)).setText("no_post");
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
