package com.app.markeet;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.app.markeet.adapter.AdapterMenuWithoutImage;
import com.app.markeet.connection.API;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.model.Menu.MenuItemImg;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.utils.NetworkCheck;
import com.app.markeet.utils.Tools;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Hashtable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMyWallet extends AppCompatActivity {

    private View parent_view;
    private RecyclerView recyclerView;
    private Call<get_product_categories> callbackCall;
    private AdapterMenuWithoutImage adapter;
    Hashtable<Object, Object> tabelvalue = new Hashtable<Object, Object>();
    ArrayList<MenuItemImg> dafmenu =  new ArrayList<MenuItemImg>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        parent_view = findViewById(android.R.id.content);
        initComponent();
        initToolbar();
        dafmenu.add(new MenuItemImg(1,"Request Topup", "Click to request topup","#ee42f4","file:///android_asset/aset/ikontombol1.jpg"));
        dafmenu.add(new MenuItemImg(2,"Waiting Your Transfer", "Confirm your transfer","#ee42f4","file:///android_asset/aset/ikontombol1.jpg"));
        dafmenu.add(new MenuItemImg(3,"Confirmed Transfer", "In waiting list for admin to review","#ee42f4","file:///android_asset/aset/ikontombol1.jpg"));
        dafmenu.add(new MenuItemImg(4,"Accepted Transfer", "List of success transfer","#ee42f4","file:///android_asset/aset/ikontombol1.jpg"));
        dafmenu.add(new MenuItemImg(5,"Canceled Transfer", "List of failed transfer","#ee42f4","file:///android_asset/aset/ikontombol1.jpg"));
        dafmenu.add(new MenuItemImg(6,"Archieve", "Request history","#ee42f4","file:///android_asset/aset/ikontombol1.jpg"));
        //requestListCategory();

        recyclerView.setVisibility(View.VISIBLE);
        adapter.setItems(dafmenu);
    }


    private void initToolbar() {
        ActionBar actionBar;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(R.string.title_activity_mywallet);
        Tools.systemBarLolipop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(ActivityMyWallet.this, 1));

        //set data and list adapter
        adapter = new AdapterMenuWithoutImage(ActivityMyWallet.this, new ArrayList<MenuItemImg>());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener(new AdapterMenuWithoutImage.OnItemClickListener() {
            @Override
            public void onItemClick(View view, MenuItemImg obj, Integer posisi) {
                Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                //ActivityCategoryDetails.navigate(ActivityMyShop.this, obj);
                switch(posisi){
                    case 0:
                        Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                        ActivityRequestTopup.navigate(ActivityMyWallet.this);
                        break;
                    case 1:
                        Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                        ActivityMyTopUp.navigate(ActivityMyWallet.this,0,obj.getCategory());
                        break;
                    case 2:
                        Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                        ActivityMyTopUp.navigate(ActivityMyWallet.this,1,"Confirmed Transfer");
                        break;
                    case 3:
                        Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                        ActivityMyTopUp.navigate(ActivityMyWallet.this,3,"Accepted Transfer");
                        break;
                    case 4:
                        Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                        ActivityMyTopUp.navigate(ActivityMyWallet.this,2,"Canceled Transfer");
                        break;
                    case 5:
                        Snackbar.make(parent_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                        ActivityMyTopUp.navigate(ActivityMyWallet.this,4,"Archieve");
                        break;
                }
            }
        });
    }

    private void requestListCategory() {

        tabelvalue.put("module", "global");
        tabelvalue.put("action", "get_product_categories");
// Using the Retrofit
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","get_product_categories");


        API api = RestAdapter2.createAPI();
        Call<get_product_categories> call = api.getProductCategories(jsonObject);

        call.enqueue(new Callback<get_product_categories>() {
            @Override
            public void onResponse(Call<get_product_categories> call, Response<get_product_categories> response) {
                get_product_categories resp = response.body();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    //adapter.setItems(resp.getCategories());

                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<get_product_categories> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(ActivityMyWallet.this)) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

}
