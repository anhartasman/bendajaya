package com.app.markeet.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.markeet.ActivityCategoryDetails;
import com.app.markeet.ActivityMain;
import com.app.markeet.MyAPI;
import com.app.markeet.R;
import com.app.markeet.ServiceGenerator;
import com.app.markeet.ThisApplication;
import com.app.markeet.adapter.AdapterCategory;
import com.app.markeet.connection.API;
import com.app.markeet.connection.IRetrofit;
import com.app.markeet.connection.RestAdapter;
import com.app.markeet.connection.RestAdapter2;
import com.app.markeet.connection.callbacks.CallbackCategory;
import com.app.markeet.model.ProductCategory.list.Category;
import com.app.markeet.model.ProductCategory.response.get_product_categories;
import com.app.markeet.utils.NetworkCheck;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Hashtable;

import needle.Needle;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCategory extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private Call<get_product_categories> callbackCall;
    private AdapterCategory adapter;
    Hashtable<Object, Object> tabelvalue = new Hashtable<Object, Object>();
    ThisApplication ta;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_category, null);
        initComponent();
        requestListCategory();
        ta = ((ThisApplication)getActivity().getApplicationContext());

        return root_view;
    }

    private void initComponent() {
        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        //set data and list adapter
        adapter = new AdapterCategory(getActivity(), new ArrayList<Category>());
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);

        adapter.setOnItemClickListener(new AdapterCategory.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Category obj) {
                Snackbar.make(root_view, obj.getCategory(), Snackbar.LENGTH_SHORT).show();
                ActivityCategoryDetails.navigate(getActivity(), obj);

            }
        });
    }


    private void requestListCategory() {

        tabelvalue.put("module", "global");
        tabelvalue.put("action", "get_product_categories");
/**
        final MyAPI myapi=new MyAPI(tabelvalue,"http://berkahniaga.com/bendajaya/public/servis","get_product_categories");

        try {
            if (myapi.get_error_code().equalsIgnoreCase("000")) {

                myapi.get_hasilambil().getString("respon_data");
                Gson gson = new GsonBuilder().create();

                final get_product_categories gpc = gson.fromJson(myapi.get_hasilteks(), get_product_categories.class);

                Log.d(getClass().getName(),"jumlah kategori product "+String.valueOf(gpc.getCategories().size()));

                recyclerView.setVisibility(View.VISIBLE);
                adapter.setItems(gpc.getCategories());

                ActivityMain.getInstance().category_load = true;
                ActivityMain.getInstance().showDataLoaded();

            }
        }catch (JSONException jsoe){

        }
 **/
// Using the Retrofit
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("module","global");
        jsonObject.addProperty("action","get_product_categories");


        API api = RestAdapter2.createAPI();
        Call<get_product_categories> call = api.getProductCategories(jsonObject);

        call.enqueue(new Callback<get_product_categories>() {
            @Override
            public void onResponse(Call<get_product_categories> call, Response<get_product_categories> response) {
                get_product_categories resp = response.body();

                if (resp != null && resp.getErrorCode().equals("000")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    ta.list_categories=resp.getCategories();
                    adapter.setItems(resp.getCategories());

                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }

            }

            @Override
            public void onFailure(Call<get_product_categories> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });

        //callbackCall = api.getProductCategories(jsonObject);

        /**
        API api = RestAdapter2.createAPI();
        callbackCall = api.getListCategory();
        callbackCall.enqueue(new Callback<CallbackCategory>() {
            @Override
            public void onResponse(Call<CallbackCategory> call, Response<CallbackCategory> response) {
                CallbackCategory resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.categories);

                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackCategory> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });
        **/

    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

}
